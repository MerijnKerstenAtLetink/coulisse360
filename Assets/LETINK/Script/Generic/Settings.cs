﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;



namespace LetinkDesign
{
    public class Settings : MonoBehaviour
    {

        public static Settings instance;
        public PlayerSettings playerSettings;

        [SerializeField]
        private bool autoSave = false;

        private string projectPath;

        public enum VideoQuality
        {
            Test,
            Low,
            Medium,
            High,
        }


        [System.Serializable]
        public class PlayerSettings
        {
            public bool playerVrMode;
            public bool vrCapable;
            public VideoQuality videoQuality;
            public bool useLensDistortionCorrection;
            public bool swipeTrackingOnly;
            public bool wifiOnly;
            public bool developerMode;
            public bool showGrid;
            public bool devGyro;
            public float singleScreenDamper;
        }


        void Awake()
        {
            instance = this;
        }

        public void Init()
        {
            projectPath = Application.persistentDataPath + "/Settings.xml";
            Load();
            CheckVRCapable();
            if(playerSettings.vrCapable == false)
            {
                playerSettings.playerVrMode = playerSettings.vrCapable;
                playerSettings.swipeTrackingOnly = !playerSettings.vrCapable;
            }        
        }

        private void CheckVRCapable()
        {
            if(playerSettings.developerMode)
            {
                playerSettings.vrCapable = playerSettings.devGyro;
                Debug.Log("Gyro mode (developer mode):  " + playerSettings.devGyro);
            }
            else
            {
                playerSettings.vrCapable = SystemInfo.supportsGyroscope;
                Debug.Log("Gyro mode (developer mode):  " + playerSettings.vrCapable);
            }
        }

        public void Load()
        {
            ReadFileSettings();
        }

        private void ReadFileSettings()
        {
            try
            {
                // xml load settings
                XmlSerializer reader = new XmlSerializer(typeof(PlayerSettings));

                FileStream xmlFile = File.Open(projectPath, FileMode.Open);
                playerSettings = reader.Deserialize(xmlFile) as PlayerSettings;

                Debug.Log("Settings file loaded ");
                xmlFile.Close();

            }
            catch (Exception)
            {
                Debug.Log("Settings file new or failed to read : ");
                InitNew();
            }

        }


        private void InitNew()
        {
            playerSettings.playerVrMode = false;
            playerSettings.videoQuality = VideoQuality.High;
            playerSettings.useLensDistortionCorrection = true;
            playerSettings.wifiOnly = false;
            playerSettings.swipeTrackingOnly = false;
            playerSettings.developerMode = false;
            playerSettings.devGyro = true;
            playerSettings.singleScreenDamper = 5f;
            Save();
        }


        public void Save()
        {
            try
            {
                XmlSerializer writer = new XmlSerializer(typeof(PlayerSettings));

                FileStream xmlFile = File.Create(projectPath);

                writer.Serialize(xmlFile, playerSettings);
                xmlFile.Close();
            }
            catch (Exception e)
            {
                Debug.Log("Could not save the settings file for some reason: " + e.Message);
            }
        }

        void OnDestroy()
        {
            if(autoSave)
            {
                Save();
            }
        }
    }
}
