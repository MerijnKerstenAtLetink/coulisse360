﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OneSignalStart : MonoBehaviour
{
    void Start()
    {
        // Enable line below to enable logging if you are having issues setting up OneSignal. (logLevel, visualLogLevel)
        //OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.INFO, OneSignal.LOG_LEVEL.INFO);
        OneSignal.Init("3dd0bc52-926b-47e0-80e2-09eaea3a7fef", "1862895642", HandleNotification, false);
    }

    // Gets called when the player opens the notification.
    private static void HandleNotification(string message, Dictionary<string, object> additionalData, bool isActive)
    {
        Debug.Log("Received notification " + message);
    }
}
