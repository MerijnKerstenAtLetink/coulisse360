﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LetinkDesign.VR;
using System.IO;
using System.Net;
using System;
using TreeNutsGames.Trace;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;

namespace LetinkDesign
{

    public class AssetLoader : MonoBehaviour
    {

        #region Properties

        public static AssetLoader instance;
        
        // online
        public bool assetLoaderReady = false;
        public bool serverIsOnline = false;
        public bool isDownloading = false;
        [SerializeField]
        private bool enableOnline = true; //warning this should be on for builds;
        [SerializeField]
        private bool disableOnlineSafe = true;  //  Should be true for builds with Online features.
                                                // enables enableOnline when its false in a build.
                                                // should be false if build realy dont need online features

        // paths
        public string serverPath;
        public string projectFolder;
        public string assetDirectory;

        private float defaultTimeOut = 10f;
        private bool timedOut = false;

        

        private string serverQualityFolder;
        private Coroutine serverCheck;

        [SerializeField]
        private List<Texture2D> textureAssets = new List<Texture2D>();

        
        private bool checking;


        [SerializeField]
        private Downloader downloader;
        [SerializeField]
        private bool useDownloader;


        #endregion Properties


        #region Init

        void Awake()
        {
            instance = this;
            
        }

        void Start()
        {
            if(enableOnline == false)
            {
               // Trace.Log("Warning: Enable Online false");
            }
            if(disableOnlineSafe)
            {
#if !UNITY_EDITOR
                enableOnline = true;
#endif
            }
        }

        public void Init()
        {
            InitPath();
            if(enableOnline)
            {
                InitOnline();
            }
        }

        private void InitOnline()
        {  
            StartCoroutine(CheckOnline());          
        }

        private void InitPath()
        {

            switch (Settings.instance.playerSettings.videoQuality)
            {

                case Settings.VideoQuality.Test:
                    serverQualityFolder = "test/";
                    break;
                case Settings.VideoQuality.Low:
                    serverQualityFolder = "low/";
                    break;
                case Settings.VideoQuality.Medium:
                    serverQualityFolder = "medium/";
                    break;
                case Settings.VideoQuality.High:
                    serverQualityFolder = "high/";
                    break;
                default:
                    break;
            }


            assetDirectory = Application.persistentDataPath + "/assets/" + serverQualityFolder;

            try
            {
                if (Directory.Exists(assetDirectory))
                {
                    assetLoaderReady = true;
          //          Debug.Log("Directory Exists: " + assetDirectory);
                }
                else
                {
                    Directory.CreateDirectory(assetDirectory);
                    Debug.Log("Directory Create: " + assetDirectory);
                    assetLoaderReady = true;
                }
            }
            catch (Exception e)
            {
                Debug.LogError("AssetLoader Failed to initialize the assetDirectory path: " + e.Message);
            }
        }

        internal void DeleteDownloads()
        {
            string deletePath = Application.persistentDataPath + "/assets/";


            try
            {
                if (Directory.Exists(deletePath + "test"))
                {
                    Debug.Log("Deleting: " + deletePath + "test");
                    Directory.Delete(deletePath + "test", true);
                }
            }
            catch (Exception e)
            {

                Trace.Log("Deleting downloads done with errors: " + e.Message);
            }


            try
            {
                if (Directory.Exists(deletePath + "low"))
                {
                    Debug.Log("Deleting: " + deletePath + "low");
                    Directory.Delete(deletePath + "low", true);
                } 
            }
            catch (Exception e)
            {

                Trace.Log("Deleting downloads done with errors: "+ e.Message);
            }

            try
            {
                if (Directory.Exists(deletePath + "medium"))
                {
                    Debug.Log("Deleting: " + deletePath + "medium");
                    Directory.Delete(deletePath + "medium", true);
                }
            }
            catch (Exception e)
            {

                Trace.Log("Deleting downloads done with errors: " + e.Message);
            }

            try
            {
                if (Directory.Exists(deletePath + "high"))
                {
                    Debug.Log("Deleting: " + deletePath + "high");
                    Directory.Delete(deletePath + "high", true);
                }
            }
            catch (Exception e)
            {

                Trace.Log("Deleting downloads done with errors: " + e.Message);
            }

            UnityEngine.SceneManagement.SceneManager.LoadScene("MainScene");
        }

        #endregion Init



        #region Online

        private IEnumerator CheckOnline()
        {

            yield return new WaitForSeconds(2f);
            StartCoroutine(CheckServerReady());
            
        }


        /// <summary>
        /// Checks the server and this project path on the server
        /// and sets the serverIsOnline if project path is valid
        /// </summary>
        /// <returns></returns>
        private IEnumerator CheckServerReady()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(serverPath);
            request.Timeout = 2000;
            
            try
            {
                using (HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
                {
                    bool isSucces = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                    serverIsOnline = isSucces;
                    Debug.Log("AssetLoader: Server is online ");
                }
            }
            catch
            {
                serverIsOnline = false;
            }
            yield return null;
        }


        public IEnumerator RequestWWW(string assetID, object sender)
        {
            isDownloading = true;
            if (!serverIsOnline)
            {
                InitOnline();
            }
            InitPath();

            Debug.Log("RequestWWW " + assetID);


            AreaSwitchLoaderCheck asSender = sender as AreaSwitchLoaderCheck;
            GameObject goSender = asSender.GetComponent<Transform>().gameObject;


            using (WWW www = new WWW(serverPath + projectFolder + serverQualityFolder + assetID))
            {
                var progress1 = www;
                while(progress1.progress != 1)
                {
                    //Debug.Log("Download: " + progress1.progress);
                    if(sender != null && goSender.activeInHierarchy)
                    {
                        asSender.UpdateProgress(progress1.progress);
                    }
                    yield return null;
                }
                //yield return www;

                if (string.IsNullOrEmpty(www.error))
                {
                    Debug.Log("Downloaded: " + assetID);
                    try
                    {
                        //var progress1 = www;
                        File.WriteAllBytes(assetDirectory + "/" + assetID, progress1.bytes);
                        if (sender != null && goSender.activeInHierarchy)
                        {
                            asSender.Reset();
                        }

                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Downloaded file write error: " + e.Message);

                        // FOR EXAMPLE LOW DISK
                        Trace.Log("Error writing the download file. Try deleting some videos!", Color.red);
                        Trace.LogColor(Color.white);
                    }
                    
                }
                else
                {
                    try
                    {
                        Debug.Log(www.error + "\r  object: " + sender.ToString() + "\r for : " + serverPath + projectFolder + serverQualityFolder + assetID);
                        if (sender == typeof(AreaSwitchLoaderCheck))
                        {
                            AreaSwitchLoaderCheck awlc = sender as AreaSwitchLoaderCheck;
                            awlc.FailedDownload();
                        }

                    }
                    catch (Exception e)
                    {

                        Debug.Log("error update state sender: " + e.Message);
                    }

                }
                www.Dispose();

                System.GC.Collect();

            }

                
            isDownloading = false;
            Resources.UnloadUnusedAssets();
        }

        internal void Download(string assetID, object sender)
        {
            if (useDownloader)
            {
                StartCoroutine(downloader.RequestStreamDownload(assetID, sender));
            }
            else
            {
                StartCoroutine(RequestWWW(assetID, sender));
            }
         }
            

#endregion Online



#region LocalFiles


        public bool GetFileByName(string fileName)
        {
            bool ret = false;

            //Debug.Log("Checking local IO for file: " + fileName);

            try
            {
                if (File.Exists(assetDirectory + "/" + fileName))
                {
                    ret = true;
              //      Debug.Log("File exists !!!");
                }
                else
                {
                    Debug.Log("File not found for: "+fileName);
                }
            }
            catch (Exception e)
            {
                Debug.Log("File not found local" + e.Message);
                throw;
            }

            return ret;
        }


        public Texture2D LoadTexture(string id)
        {
            Texture2D ret = null;
            int index = -1;

            foreach (Texture2D tex in textureAssets)
            {
                index++;
                if(tex != null)
                {
                    if (tex.name == id)
                    {
                        ret = textureAssets[index];
                        break;
                    }
                }
                else
                {
                    Debug.Log("Asking for local texture: " + id + " but the assetloader does not have it");
                }
                
            }

            return ret;
        }

        #endregion LocalFiles



        #region AssetBundles

        public static AssetBundle Load(string assetID)
        {
            AssetBundle ret = null;

            Debug.Log("AssetLoader was asked to load " + assetID);

            // check if it is in the cache

                // true false.

            // if so the return the AssetBundel from the cache

                //ret = cacheManager.assetID;

            // else start loading the AssetBundle with ID assetID

            instance.StartCoroutine(instance.AssetLoaderTimeOut(instance.defaultTimeOut));

            instance.StartCoroutine(instance.AssetLoaderLoadAssetWWW(assetID));


            return ret;

        }

        private IEnumerator AssetLoaderTimeOut(float timeToTimeOut)
        {
            yield return new WaitForSeconds(timeToTimeOut);
            TimeOut();
        }


        private void TimeOut()
        {
            Debug.Log("TimeOut happend!");
        }


        private IEnumerator AssetLoaderLoadAssetWWW(string assetID)
        {
            WWW www = null;
            while(!timedOut)
            {
                yield return www;
            }
            
        }

        #endregion AssetBundles

    }
}
