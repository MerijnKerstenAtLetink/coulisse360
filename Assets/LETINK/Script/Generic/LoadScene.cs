﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using LetinkDesign.UI;
using System;

public class LoadScene : MonoBehaviour
{


    #region Properties

    private LoadScene instance;

    public bool skipIntro = true;
    private bool skipped = false;

    public string sceneName;

    [SerializeField]
    private MediaPlayerCtrl videoPlayer;

    [SerializeField]
    private UIFader fader;

    private Coroutine coroutine;


    #endregion Properties



    #region Init


    private void Start()
    {

#if !UNITY_EDITOR
        skipIntro = false;
#endif

        if (skipIntro)
        {
            LoadNextScene();
            return;
        }

        instance = this;

        if(videoPlayer != null)
        {
            //coroutine = StartCoroutine(RunVideo());
        }
    }


    #endregion Init



    #region Logic

    public void SkipIntro()
    {
        skipped = true;
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
           
        }
        if(videoPlayer != null)
        {
            videoPlayer.UnLoad();
        }
    }


    private void LoadNextScene()
    {
        if(string.IsNullOrEmpty(sceneName))
        {
            sceneName = "MainScene";
        }

        SceneManager.LoadScene(sceneName);
    }

    private IEnumerator RunVideo()
    {
        if(videoPlayer)
        {
            videoPlayer.Play();
        }
        
       // videoPlayer.OnEnd += EndOfVideo();

        yield return new WaitForSeconds(0.5f);

        while(!skipped && videoPlayer.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING)
        {
            if(skipped)
            {
                break;
            }
            else
            {
                yield return null;
            }
        }
        LoadNextScene();
    }

    public void LoadSceneNow()
    {
        LoadNextScene();
    }

    public void LoadSceneDelayed(float wait)
    {
        instance.StartCoroutine(LoadSceneDelay(wait));
    }

    private IEnumerator LoadSceneDelay(float wait)
    {
        if (fader)
        {
            fader.StartFadeOut(wait);
        }

        yield return new WaitForSeconds(wait);
        LoadNextScene();
    }

    private void EndOfVideo()
    {
        skipped = true;
        //return null;
    }

    #endregion Logic

}
