﻿using UnityEngine;
using System.Collections;
using TreeNutsGames.Trace;
using LetinkDesign.VR;
using System;
using LetinkDesign.UI;

namespace LetinkDesign
{
    public class GameMain : MonoBehaviour
    {

        public static GameMain instance;
        private Settings mainSettings;

        private Coroutine bootRoutine;
        private Coroutine goBackRoutine;
        private bool bootError = false;

        public static bool inited = false;
        private static string qurl;
        private bool gobackReady = true;

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                bootRoutine = StartCoroutine(BootUp());
                StartCoroutine(BootTimeOut());
            }
        }



        private IEnumerator BootTimeOut()
        {
            yield return new WaitForSeconds(5f);

            yield return null;
        }



        private IEnumerator BootUp()
        {

            while (Settings.instance == null)
            {
                yield return null;
            }

           // Debug.Log("Settings " + Time.timeSinceLevelLoad);

            while (AssetLoader.instance == null)
            {
               
                yield return null;
            }

            //Debug.Log("AssetLoader " + Time.timeSinceLevelLoad);

            while (VrModeManager.instance == null)
            {
                yield return null;
            }

          //  Debug.Log("VR ModeManager " + Time.timeSinceLevelLoad);

            while (Cardboard.SDK == null)
            {
                yield return null;
            }

          //  Debug.Log("Cardboard SDK " + Time.timeSinceLevelLoad);

            if (!bootError)
            {
                Settings.instance.Init();
                yield return null;
                AssetLoader.instance.Init();
                VrModeManager.instance.Init();
                VrModeManager.instance.UseSwipeTracking(Settings.instance.playerSettings.swipeTrackingOnly && !Settings.instance.playerSettings.playerVrMode);
                GetComponent<LetinkDesing.CameraTouchMouse>().Init();

                if(!inited)
                {
                    inited = true;
                }
                else
                {
                    Debug.Log("Hot Reset");
                }

                Cardboard.SDK.OnBackButton  += BackButton;

            }
            else
            {
                Trace.Log("Boot error", Color.red);
            }
            yield return null;
        }

        internal static void QueUrl(string url)
        {// temp will be in www section
            qurl = url;
        }

        void Update()
        {
            UpdateSpheres(); // Letink
        }

        private void UpdateSpheres()
        {
            if (PanoramicManager.instance)
            {
                PanoramicManager.instance.UpdateSpheres();
            }
        }


        private void BackButton()
        {
            UI_Manager.uiMode = false;
            if (gobackReady)
            {
                gobackReady = false;
                if(goBackRoutine != null)
                {
                    StopCoroutine(goBackRoutine);
                    goBackRoutine = null;
                }
                goBackRoutine = StartCoroutine(GoBack());
            }

        }

        private IEnumerator GoBack()
        {
                
            if (VrModeManager.instance.vrMode == VRMode.Single)
            {
                if(SceneManager.instance.isSwitching == false)
                {
                    SceneManager.instance.NavigateBack();
                }
                else
                {
                   // Trace.Log("Nope");
                }


            }
            else
            {
                VrModeManager.instance.SetVRMode(false, true);
              //  Trace.Log("Switch to singlescreen");
            }
            yield return new WaitForSeconds(2f);
            gobackReady = true;
           
        }

        void OnDestroy()
        {
            StopAllCoroutines();
        }

        void OnApplicationQuit()
        {
            Cardboard.SDK.OnBackButton -= BackButton;
            if (!string.IsNullOrEmpty(qurl))
            {
                Application.OpenURL(qurl);
            }
        }

    }
}
