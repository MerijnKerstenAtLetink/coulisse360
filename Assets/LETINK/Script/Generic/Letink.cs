﻿


namespace LetinkDesign
{
    public enum FileState
    {
        Unknown,
        NotReady,
        Online,
        Downloading,
        Ready,
        Failed
    }

    public enum AssetLocationType
    {
        Unknown,
        StreamingAsset,
        Downloadable,
        Youtube,
    }
}




namespace LetinkDesign.VR
{
    public enum SceneType
    {
        Panoramic,
        PanoramicStereoDualMono,
        PanoramicStereoTopDown,
        PanoramicStereoSideBySide,
        VideoMono,
        VideoStereoTopDown,
        VideoStereoSideBySide,
        ThreeD,
    }

    public enum ModeProperty
    {
        None,
        SingleAndVR,
        SingleOnly,
        VROnly
    }


    public enum VRMode
    {
        None,
        VR,
        Single
    }
}