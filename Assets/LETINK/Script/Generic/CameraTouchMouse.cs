﻿using UnityEngine;
using System.Collections;
using TreeNutsGames.Trace;
using LetinkDesign.VR;
using LetinkDesign.UI;

namespace LetinkDesing
{
    public class CameraTouchMouse : MonoBehaviour
    {
        [SerializeField]
        private Transform rotateObject;

        private Vector2 lastMouseDelta;

        public bool mouseEnabled = false;
        public bool touchEnabled = false;
        [SerializeField]
        private float speedX;
        [SerializeField]
        private float speedY;

        private float ySpeedRotation;
        private float xSpeedRotation;
        [SerializeField]
        private float drag;

        float driveY;
        private float driveX;
        [SerializeField]
        private float treshholdTx = 1f;
        [SerializeField]
        private float treshholdTy = 1f;
        [SerializeField]
        private float treshholdMx = 1f;
        [SerializeField]
        private float treshholdMy = 1f;

        public void TouchSwipeActive(bool touchSwipe)
        {
            touchEnabled = touchSwipe;
            mouseEnabled = touchSwipe;
            Init();
        }


        public void Init()
        {
            if(touchEnabled == false && mouseEnabled == false)
            {
                this.enabled = false;
            }
            else
            {
                this.enabled = true;
            }
        }



        void Update()
        {
            
            if (rotateObject != null)
            {

                float deltaY = 0f;
                float deltaX = 0f;

                if(Input.touchCount > 0)
                {
                   // Trace.Log("TouchEnabled");
                    touchEnabled = true;
                    mouseEnabled = false;
                }
                else if(Input.GetMouseButton(0))
                {
                  //  Trace.Log("MouseEnabled");
                    mouseEnabled = true;
                    touchEnabled = false;
                }


                
                if(UI_Manager.uiMode == false)
                {
                    #region GetTouch Swipe
                    if (touchEnabled)
                    {
                        if (Input.touchCount > 0)
                        {

                            Touch touch = Input.GetTouch(0);
                            if (touch.phase == TouchPhase.Moved && touch.phase != TouchPhase.Ended)
                            {
                                deltaY = -touch.deltaPosition.x;
                                deltaX = -touch.deltaPosition.y;

                                if(Mathf.Abs(deltaX) > treshholdTx || Mathf.Abs(deltaY) > treshholdTy)
                                {
                                    driveX = deltaX * speedX;
                                    driveY = deltaY * speedY;
                                    ySpeedRotation += driveY;
                                    xSpeedRotation += driveX;
                                    VrModeManager.instance.isMovingCam = true;
                                }
                            }
                        }
                        else if (Input.touchCount == 0)
                        {
                            // reduce drive power with drag over time
                            driveY = Mathf.Lerp(driveY, 0f, drag * Time.deltaTime);
                            driveX = Mathf.Lerp(driveX, 0f, drag * Time.deltaTime);

                            // apply driving power
                            ySpeedRotation += driveY;
                            xSpeedRotation += driveX;
                            VrModeManager.instance.isMovingCam = false;
                        }

                    }

                    #endregion


                    #region GetMouse Swipe
                    if (mouseEnabled)
                    {
                        if (Input.GetMouseButton(0))
                        {
                            // movement per frame
                            deltaY = lastMouseDelta.x - Input.mousePosition.x;
                            lastMouseDelta.x = Input.mousePosition.x;

                            deltaX = lastMouseDelta.y - Input.mousePosition.y;
                            lastMouseDelta.y = Input.mousePosition.y;

                            if (Mathf.Abs(deltaX) > treshholdMx || Mathf.Abs(deltaY) > treshholdMy)
                            {
                                // driving power
                                driveY = deltaY * speedY;
                                driveX = deltaX * speedX;

                                // apply driving power
                                ySpeedRotation += driveY;
                                xSpeedRotation += driveX;
                                VrModeManager.instance.isMovingCam = true;
                            }
                        }
                        else
                        {
                            VrModeManager.instance.isMovingCam = false;

                            // update lastMousePos to be delta 0 for next frame
                            lastMouseDelta.x = Input.mousePosition.x;
                            lastMouseDelta.y = Input.mousePosition.y;

                            // reduce drive power with drag over time
                            driveY = Mathf.Lerp(driveY, 0f, drag * Time.deltaTime);
                            driveX = Mathf.Lerp(driveX, 0f, drag * Time.deltaTime);

                            // apply driving power
                            ySpeedRotation += driveY;
                            xSpeedRotation += driveX;
                        }
                    }
                    #endregion
                }



                xSpeedRotation = Mathf.Clamp(xSpeedRotation, -75f , 75f);

                // apply rotation from driving power
                rotateObject.eulerAngles = new Vector3(-xSpeedRotation, ySpeedRotation,0f);

                
            }
        }
    }
}




/*
 if (mouseEnabled)
                {
                    float deltaY = 0f;

                    if (Input.GetMouseButton(0))
                    {
                        deltaY = lastMouseDelta.x - Input.mousePosition.x;
                        lastMouseDelta.x = Input.mousePosition.x;

                        driveY = deltaY * speedY;

                        ySpeedRotation  += driveY;
                    }
                    else
                    {
                        lastMouseDelta.x = Input.mousePosition.x;
                        driveY = Mathf.Lerp(driveY, 0f, drag * Time.deltaTime);
                        ySpeedRotation += driveY;  
                    }

                    rotateObject.localEulerAngles = new Vector3(0f, ySpeedRotation, 0f);
                }


    */