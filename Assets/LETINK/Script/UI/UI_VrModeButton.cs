﻿using UnityEngine;
using System.Collections;
using LetinkDesign;
using UnityEngine.EventSystems;
using System;

public class UI_VrModeButton : MonoBehaviour {


    public bool vrMode = false;



    private void Update()
    {
        if (vrMode == Settings.instance.playerSettings.playerVrMode)
        {
            EventSystem events = EventSystem.current;
            events.SetSelectedGameObject(gameObject);
        }
    }
}
