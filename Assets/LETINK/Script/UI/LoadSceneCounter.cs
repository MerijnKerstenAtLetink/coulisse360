﻿using UnityEngine;
using System.Collections;

public class LoadSceneCounter : MonoBehaviour {

    [SerializeField]
    private float timer;
    public GameObject StartInstructionScreen;

    void Update()
    {
        if (timer > 1f)
        {
            timer -= Time.deltaTime;
        }

        if (timer < 1f)
        {            
            StartInstructionScreen.GetComponent<SettingsPanel>().Next();
        }
    }
}
