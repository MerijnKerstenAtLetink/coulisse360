﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class UI_TextFade : MonoBehaviour
{

    private Text text;
    private Outline outline;
    [SerializeField]
    private float fadeTime;
    [SerializeField]
    private float preWait = 0f;
    private float alpha = 0f;
    private Color color;
    private float speed;
    [SerializeField]
    private float waitTime;


    void OnEnable()
    {
        text = GetComponent<Text>();
        outline = GetComponent<Outline>();
        color = text.color;
        color.a = 0f;
        text.color = color;
        this.enabled = true;
        StartCoroutine(Fade(fadeTime));
        Debug.Log("Balal");
    }

    #region Fade



    private IEnumerator Fade(float targetTime)
    {
        yield return new WaitForSeconds(preWait);

        speed = 1f / targetTime;
        color.a = text.color.a;
        alpha = color.a;

        while (alpha < 1f)
        {
            alpha += Time.deltaTime * speed;
            color.a = alpha;
            text.color = color;
            yield return null;
        }

        yield return new WaitForSeconds(waitTime);

        while (alpha > 0f)
        {
            alpha -= Time.deltaTime * speed;
            color.a = alpha;
            text.color = color;
            yield return null;
        }
    }

    void OnDestroy()
    {
        StopAllCoroutines();
    }

    #endregion Fade

}
