﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


/* 

 Usage:

 Trace.Log(log);                 // normal Log
 Trace.Log(log, Color.red);      // log with onetime other color   


 Optional Settings:

 Trace.LogColor(Color.green);    // change logColor permantent
 Trace.BackgroundAlpha(1f);      // set log background transparentie
 Trace.LogLines(1);              // amount of lines you want to see
 Trace.LogLineTime(5f);          // length of time in seconds for a line te be visible
 Trace.ScrollLines(true);        // Enable/Disable scrolling the lines (clear lines)
 Trace.Reset();                  // Reset trace to default values

*/

namespace TreeNutsGames.Trace
{
	public class Trace : MonoBehaviour
	{

		#region Component Properties

		public static Trace instance;
		public GameObject LogModule;

		private GameObject logInstance;

		private Text logText;
		private Image logBackground;

		#endregion



		#region Log Properties

		private float runTimer = 3f;
		private float markTimer;

		private List<string> lines = new List<string>();

		private static Color logLineColor = Color.white;
		private static float backGroundAlpha = 0f;
		private static float logLineTime = 3f;
		private static int maxLines = -1;
		private static bool scrollLines = true;

		#endregion



		#region Init

		/// <summary>
		/// MonoBehavior Awake
		/// </summary>
		private void Awake()
		{
			instance = this;
			Init();
		}

		/// <summary>
		/// Init values
		/// </summary>
		private void Init()
		{
			runTimer = 1f;
			maxLines = -1;
			logLineColor = Color.white;
			backGroundAlpha = 0f;
			logLineTime = 3f;
			scrollLines = false;
			LogLines(10);
			LogLineTime(7);
		}

        void Start()
        {
           // Log("Trace log active !");
        }

		#endregion



		#region Log functions


		/// <summary>
		/// Basic Log  just add water... I mean string
		/// </summary>
		/// <param name="logtext"></param>
		public static void Log(object logtext)
		{
            if (instance)
            {
                instance.InstantiateLog(logtext, logLineTime);
            }
            else
                Debug.Log("Trace (no instance): " + logtext);
            
		}



		/// <summary>
		/// Temp use colorlines for log. More usefull in single line logs
		/// </summary>
		/// <param name="logtext"></param>
		/// <param name="logColor"></param>
		public static void Log(string logtext, Color logColor)
		{
			Color cacheColor = logLineColor;

			LogColor(logColor);
			instance.InstantiateLog(logtext, logLineTime);
			logLineColor = cacheColor;
		}


		public static void Clear()
		{
			if (instance != null)
			{
				instance.lines.Clear();
			}
		}
		#endregion



		#region Change log properties

		/// <summary>
		/// Sets the logLinesColor
		/// </summary>
		/// <param name="logColor"></param>
		public static void LogColor(Color logColor)
		{
			logLineColor = logColor;
		}

		/// <summary>
		///  Sets the background transparentie
		/// </summary>
		/// <param name="bgAlpha"></param>
		public static void BackgroundAlpha(float bgAlpha)
		{
			backGroundAlpha = bgAlpha;
		}

		/// <summary>
		/// Sets the logTime to show. Note that lines will be cleared if overridden by next call.
		/// </summary>
		/// <param name="logTime"></param>
		public static void LogLineTime(float logTime)
		{
			logLineTime = logTime;
			instance.enabled = !(logLineTime == -1);
		}

		public static void LogLines(int lineCount)
		{
			maxLines = lineCount;
		}

		public static void ScrollLines(bool scroll)
		{
			scrollLines = scroll;
		}


		/// <summary>
		/// Resets to default values
		/// </summary>
		public static void Reset()
		{
			instance.Init();
		}


		#endregion



		#region Update and Run log

		private void InstantiateLog(object logtext, float adTime)
		{
			// fetch items if null
			if (logInstance == null)
			{
				logInstance = Instantiate(LogModule);
				logText = logInstance.GetComponentInChildren<Text>();
				logBackground = logInstance.GetComponentInChildren<Image>();
			}

			markTimer = Time.timeSinceLevelLoad;
			// build the log lines
			if (lines.Count >= maxLines)
			{
				lines.RemoveAt(0);
			}
			else if (runTimer < logLineTime)
			{
				runTimer = logLineTime;
			}


			lines.Add("" + logtext);
			logText.text = ListToText();


			// set background alpha
			Color bga = new Color();
			bga.a = backGroundAlpha;
			logBackground.color = bga;

			// set lines color
			Color lc = new Color();
			lc = logLineColor;
			logText.color = lc;

			//if (scrollLines)
		//	{
				this.enabled = true;
		//	}
		}


		private string ListToText()
		{
			string ret = "";

			foreach (var item in lines)
			{
				ret += item + "\n";
			}

			int LIO = ret.LastIndexOf("\n");
			if (LIO != -1)
			{
				ret = ret.Substring(0, LIO);
			}

			return ret;
		}


		private void Update()
		{
			if (runTimer > 0)
			{
				runTimer -= Time.deltaTime;
				if (Time.timeSinceLevelLoad - markTimer > logLineTime)
				{
					if (lines.Count > 0)
					{
						markTimer = Time.timeSinceLevelLoad;
						lines.RemoveAt(0);
						logText.text = ListToText();
					}
				}
			}
			else
			{
				this.enabled = false;
				if (logInstance != null)
				{
					Destroy(logInstance.gameObject);
				}

				lines.Clear();
				logInstance = null;
			}
		}

		#endregion
	}
}

