﻿using UnityEngine;
using System.Collections.Generic;

namespace LetinkDesign.UI
{
    public class UI_Manager : MonoBehaviour
    {

        #region Properties

        public static UI_Manager instance;

        public static bool uiMode = false;

        public List<UIElement> uiStack = new List<UIElement>();


        public static Transform uiMainCanvas
        {
            get { return instance.uiCanvas.transform; }
            
        }


        [SerializeField]
        private Canvas uiCanvas;

        #endregion Properties



        #region Init

        void Awake()
        {
            instance = this;
        }



        void Start()
        {

        }

        #endregion Init



        #region Update


        //void Update()
        //{

        //}

        #endregion



        #region UIElements Listhandling


        public static void AddUIElement(UIElement uiElement)
        {
            instance.uiStack.Insert(0, uiElement);
        }



        public static void RemoveUIElement(UIElement uiElement)
        {
            instance.uiStack.Remove(uiElement);
        }


        #endregion UIElements Listhandling



        #region InGameMenu

        private void ShowIngameUI(string mode)
        {
            if(mode == "default") // TODO: to enum
            {
                // InGameUI.ShowDefault Ingame UI
            }
        }

        #endregion InGameMenu


    }
}

