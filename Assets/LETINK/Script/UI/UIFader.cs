﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;


namespace LetinkDesign.UI
{
    public class UIFader : MonoBehaviour
    {

        #region Properties

        public static UIFader instance;

        private float alpha;
        private float timer = 0f;
        private bool done = false;

        private float speed;

        [SerializeField]
        private bool startOpague = true;

        [SerializeField]
        private Image faderImage;
        private Color color;

        [SerializeField]
        private bool autoFadeIn = false;

        public static bool busy = false;
        private bool fadeOutOnly;



        #endregion Properties



        #region Event Delegates

        // 1- Define a delegate
        // 2- Define an event based on that delegate
        // 3- Raise the event

        //1
        public delegate void FaderAtHalfWayEventHandler(UIFader fader, EventArgs e);  // (object sender, EventArgs e)

        //2
        public event FaderAtHalfWayEventHandler FaderAtHalf;

        #endregion Even Delegates



        #region Init

        private void Awake()
        {
            instance = this;
            color = faderImage.color;
            color.a = startOpague ?  1f : 0f;
            faderImage.color = color;

            busy = false;

            if (autoFadeIn)
            {
               // StartFadeIn(2f);
            }
        }

        internal void Reset()
        {
            StopAllCoroutines();
            color.a = 0f;
            faderImage.color = color;
        }


        #endregion  Init



        #region Fade


        public void StartFadeInOut(float targetTime)
        {

            StopAllCoroutines();
            fadeOutOnly = false;
            busy = true;
            StartCoroutine(Fade(targetTime));

        }

        public void StartFadeIn(float targetTime)
        {
            StopAllCoroutines();
            fadeOutOnly = false;
            color.a = 1f;
            faderImage.color = color;
            busy = true;
            StartCoroutine(Fade(targetTime));
        }


        public void StartFadeOut(float targetTime)
        {
            StopAllCoroutines();
            color.a = 0f;
            fadeOutOnly = true;
            faderImage.color = color;
            busy = true;
            StartCoroutine(Fade(targetTime));
        }


        private IEnumerator Fade(float targetTime)
        {
            speed = 1f / targetTime;
            color.a = faderImage.color.a;
            alpha = color.a;

            while (alpha < 1f)
            {
                alpha += Time.deltaTime * speed;
                color.a = alpha;
                faderImage.color = color;
                //Debug.Log("Fadin Stuck");
                yield return null;
            }

            //Debug.Log("HalfWayEvent");
            OnFaderAtHalf();

            if(fadeOutOnly)
            {
                yield break;
            }


            while (alpha > 0f)
            {
                alpha -= Time.deltaTime * speed;
                color.a = alpha;
                faderImage.color = color;
               // Debug.Log("Fadout Stuck");
                yield return null;
            }

            fadeOutOnly = false;
            busy = false;
        }

        #endregion Fade



        #region Event

        protected virtual void OnFaderAtHalf()
        {
            if (FaderAtHalf != null)         //
            {
                FaderAtHalf(this, EventArgs.Empty);
            }
        }


        #endregion Event



        #region Test

        [ContextMenu("TestFader")]
        public void TestFader()
        {
            StartFadeInOut(2f);
        }

        #endregion Test

    }
}

