﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

// this can be used as a app start image cycle or as UI 3D banner with ads
// todo: public list adding or load from assetBundles.

namespace LetinkDesign.UI
{
    public class UI_ShowImages : MonoBehaviour
    {

        
        #region Properties


        public List<InfoImage> infoImageList = new List<InfoImage>();

        public GameObject msgObjOnDone; // temp dirty

        [SerializeField]
        private Image imageDisplay;

        [SerializeField]
        private Image backgroundImage;

        [SerializeField]
        private Image faderImage;
        private Color faderColor;

        [SerializeField]
        private Text displayText;

        public float fadeSpeed;
        public int loopCount = 1;  // -1 = loop infinite
        private bool canceled = false;

        private float duration = 3f;
        private float t = 0f;
        private Color coulisse = new Color(0.4f, 0.4f, 0.4f);

        [System.Serializable]
        public class InfoImage
        {
            public string name;
            public Image image;
            public Sprite sprite;
            public Vector2 size;
            public string description;
            public float showDuration;
            public Color backgroundColor;
            public Color faderColor;
        }


        #endregion Properties


        #region Init


        private void Start()
        {
            if (displayText != null)
            {
                displayText.text = "";
            }
            StartCoroutine(ShowImages());
        }


        #endregion Init



        #region Show Images  

        private IEnumerator ShowImages()
        {
            foreach(InfoImage infoImage in infoImageList)
            {
                faderColor = infoImage.faderColor;
                faderColor.a = 0f;

                Color imageDisplayColor = imageDisplay.color;

                if (faderImage != null)
                {
                    while (faderColor.a < 1f)
                    {
                        if (canceled)
                        {
                            break;
                        }
                        faderColor.a += Time.deltaTime * fadeSpeed;
                        faderImage.color = faderColor;
                        yield return null;
                    }
                }

                if (backgroundImage != null)
                {
                    backgroundImage.color = infoImage.backgroundColor;
                }


                if (imageDisplay != null)
                {
                    RectTransform rt = imageDisplay.rectTransform;

                    Vector2 size = infoImage.size;

                    rt.sizeDelta = size;

                    if (infoImage.image != null)
                    {
                        imageDisplay = infoImage.image;
                        imageDisplayColor.a = 1f;
                    }
                    else if (infoImage.sprite != null)
                    {
                        imageDisplay.sprite = infoImage.sprite;
                        imageDisplayColor.a = 1f;
                    }
                    else
                    {
                        imageDisplayColor.a = 0f;  
                    }
                    imageDisplay.color = imageDisplayColor;
                }

                if(displayText != null)
                {
                    if (!string.IsNullOrEmpty(infoImage.description))
                    {
                        displayText.text = infoImage.description;
                    }
                    else
                    {
                        displayText.text = "";
                    }
                }

                if (faderImage != null)
                {
                    while (faderColor.a > 0f)
                    {
                        if (canceled)
                        {
                            break;
                        }
                        faderColor.a -= Time.deltaTime * fadeSpeed;
                        faderImage.color = faderColor;
                        yield return null;
                    }
                }

                float showTime = infoImage.showDuration;
                while(showTime > 0f)
                {
                    if (t < 1f)
                    {
                        t += Time.deltaTime / duration;
                    }
                    if (showTime <= 2.5f)
                    {
                        imageDisplay.color = Color.Lerp(coulisse, Color.white, t);
                    }
                    if (canceled)
                    {
                        break;
                    }
                    showTime -= Time.deltaTime;
                    yield return null;
                }
            }
            yield return null;

            if (loopCount != -1)
            {
                loopCount--;
            }

            if (loopCount == 0 || loopCount < -1)
            {
                ShowImagesDone();
            }
            else
            {
                if(!canceled)
                {
                    Restart();
                }
            } 
        }


        #endregion Show Images  



        #region Loop End Destroy


        private void Restart()
        {
            StartCoroutine(ShowImages());
        }

        public void ShowImagesDone()
        {
            msgObjOnDone.SendMessage("LoadSceneNow");
        }

        private void OnDestroy()
        {
            canceled = true;
        }


        #endregion Loop End Destroy


    }
}
