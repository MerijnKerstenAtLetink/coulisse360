﻿using UnityEngine;
using System.Collections;
using LetinkDesign;
using UnityEngine.EventSystems;
using System;

public class UI_QualityButton : MonoBehaviour {


    public Settings.VideoQuality quality;


    void Update ()
    {
        if (quality == Settings.instance.playerSettings.videoQuality)
        {
            EventSystem events = EventSystem.current;
            events.SetSelectedGameObject(gameObject);
        }
    }

 
}
