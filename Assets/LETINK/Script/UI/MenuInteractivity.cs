﻿using UnityEngine;
using System.Collections;


public class MenuInteractivity : MonoBehaviour {

    public GameObject[] menuItem;
    //public GameObject[] subMenuItem;

    void Start () {

    }

    private void DisablePanels()
    {
        for (int i = 0; i < menuItem.Length; i++)
        {
            menuItem[i].SetActive(false);
        }
    }

    public void GoDo()
    {
        Debug.Log("godo activated");
    }

    public void Recenter()
    {
        Cardboard.SDK.Recenter();
    }

}