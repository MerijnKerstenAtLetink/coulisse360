﻿using UnityEngine;
using System.Collections;
using LetinkDesign;
using System;
using TreeNutsGames.Trace;

public class SettingsPanel : MonoBehaviour {
    private bool skipCardBoardInfo;

    [SerializeField]
    private GameObject[] vrAbleMenus;

    [SerializeField]
    private GameObject[] nonVrAbleMenus;

    [SerializeField]
    private int currentItem = 0;

    public int skipAtInfoForward;
    public int cardboardInfoCount;
    public int skipAtInfoBack;
    

    void Start ()
    {
        Settings.instance.Init();
	}
    
    void Update()
    {
        if(currentItem == 3)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Ended && touch.tapCount == 5)
                {
                    if(Settings.instance.playerSettings.developerMode)
                    {
                        Settings.instance.playerSettings.developerMode = false;
                        Trace.Log("Developer mode disabled");
                    }
                    else
                    {
                        Settings.instance.playerSettings.developerMode = true;
                        Trace.Log("You found the developer mode ;)");
                    }                    
                }
            }
        }
    }


    public void Next()
    {
        DisableAll();
        currentItem++;

        if(Settings.instance.playerSettings.vrCapable)
        {
            if (currentItem == skipAtInfoForward && Settings.instance.playerSettings.playerVrMode == false)
            {
                currentItem += cardboardInfoCount;
            }
            vrAbleMenus[currentItem].SetActive(true);
        }
        else
        {
            nonVrAbleMenus[currentItem].SetActive(true);
        }       
    }

    public void Back()
    {
        DisableAll();
        currentItem--;
        if (Settings.instance.playerSettings.vrCapable)
        {
            if (currentItem == skipAtInfoBack && Settings.instance.playerSettings.playerVrMode == false)
            {
                currentItem -= cardboardInfoCount;
            }
            vrAbleMenus[currentItem].SetActive(true);
        }
        else
        {
            nonVrAbleMenus[currentItem].SetActive(true);
        }
    }

    private void DisableAll()
    {
        foreach (GameObject item in vrAbleMenus)
        {
            item.SetActive(false);
        }

        foreach (GameObject item in nonVrAbleMenus)
        {
            item.SetActive(false);
        }
    }
    	
    public void SetVideoQuality(int videoQuality)
    {
        Settings.instance.playerSettings.videoQuality = (Settings.VideoQuality)videoQuality;
    }

    public void SetVrMode(bool vrMode)
    {
        Settings.instance.playerSettings.playerVrMode = vrMode;
    }

    public void WifiOnly(bool wifi)
    {
        Settings.instance.playerSettings.wifiOnly = wifi;
    }

    public void UseLensCorrection(bool useCorrection)
    {
        Settings.instance.playerSettings.useLensDistortionCorrection = useCorrection;
    }

    public void ToggleInfoWindow(GameObject infoWindow)
    {       
        infoWindow.SetActive(!infoWindow.activeInHierarchy);
    }


    public void EndSettingsPanel()
    {
        Settings.instance.Save();
    }
}
