﻿using UnityEngine;
using System.Collections;


namespace LetinkDesign.UI
{
    public class UIElement : MonoBehaviour
    {

        #region Properties

        public bool isBlocking = false;

        #endregion Properties



        #region Init

        public void InitUIElement()
        {
            StartCoroutine(Init());
        }

        private IEnumerator Init()
        {
            yield return null;
            UI_Manager.AddUIElement(this);
            
        }

        #endregion Init



        #region End


        private void OnDestroy()
        {
            EndUIElement();
        }


        /// <summary>
        /// Ends this UIElement
        /// Occurs at OnDestroy or when called public
        /// Unlists this element from uiStack in UI_Manager
        /// </summary>
        public void EndUIElement()
        {
            UI_Manager.RemoveUIElement(this);
        }

        #endregion End

    }
}

