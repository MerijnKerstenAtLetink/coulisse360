﻿using UnityEngine;
using System.Collections;
using LetinkDesign.VR;
using LetinkDesign.UI;
using TreeNutsGames.Trace;
using LetinkDesign;
using System.Collections.Generic;
using System;

public class UI_Ingame : MonoBehaviour
{

    [SerializeField]
    private List<GameObject> menuList;
    [SerializeField]
    private string activeMenu = "";

    [SerializeField]
    private GameObject CreditsMenu;


    [SerializeField]
    private GameObject developerButton;


    void Start()
    {
        CallMenu("");
    }


    public void VrMode(bool vr)
    {
        VrModeManager.instance.SetVRMode(vr, true);
    }

    public void ToggleTrackMode()
    {
        Settings.instance.playerSettings.swipeTrackingOnly = !Settings.instance.playerSettings.swipeTrackingOnly;
        VrModeManager.instance.UseSwipeTracking(Settings.instance.playerSettings.swipeTrackingOnly);
    }

    public void CallMainMenu(string menuName)
    {
        if (activeMenu == "")
        {
            CallMenu(menuName);
        }
        else
        {
            DissableAll();
        }
    }

    public void CallMenu(string menuName)
    {
        DissableAll();

        if (developerButton != null)
        developerButton.SetActive(Settings.instance.playerSettings.developerMode);

        foreach (GameObject menuItem in menuList)
        {
            if (menuItem != null)
            {
                if (menuItem.name == menuName)
                {
                    menuItem.SetActive(true);
                    activeMenu = menuItem.name;
                    UI_Manager.uiMode = true;
                    break;
                }
            }
        }
    }


    private void DissableAll()
    {
        foreach (GameObject menuItem in menuList)
        {
            if (menuItem != null)
            {
                menuItem.SetActive(false);
                activeMenu = "";
            }
        }
        UI_Manager.uiMode = false;
    }

    public void OpenUrl(string url)
    {
        Application.OpenURL(url);
    }
   

    //void LateUpdate()
    //{
    //    if (activeMenu != "")
    //    {
    //        if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) || Input.GetMouseButtonUp(0))
    //        {
    //            DissableAll();
    //        }
    //    }
    //}

    public void Recenter()
    {
        Cardboard.SDK.Recenter();
    }

    public void Home()
    {
        StartCoroutine(ReloadHome());
    }

    public IEnumerator ReloadHome()
    {
        UIFader.instance.StartFadeOut(1f);
        yield return new WaitForSeconds(1f);
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainScene");
    }

    public void ToggleNoGyro()
    {
        Settings.instance.playerSettings.vrCapable = !Settings.instance.playerSettings.vrCapable;
        Settings.instance.playerSettings.devGyro = Settings.instance.playerSettings.vrCapable;
        Trace.Log("Vr Capable = " + Settings.instance.playerSettings.vrCapable + ".  Restart app to aply");
       
    }

    public void RestartApp()
    {
        Settings.instance.Save();
        UnityEngine.SceneManagement.SceneManager.LoadScene("VideoFullScreen");
    }
   

    public void SetVideoQuality(int quality)
    {
        Settings.instance.playerSettings.videoQuality = (Settings.VideoQuality)quality;
        if(Settings.instance.playerSettings.developerMode)
        {
            Trace.Log("Video quality set to: " + Settings.instance.playerSettings.videoQuality.ToString() + ".  Restart app to aply and skip choosing a video quality");
        }
        else
        {
            //Trace.Log("Video quality set to: " + Settings.instance.playerSettings.videoQuality.ToString() + ".  Reload to aply");
        }
        
    }

    public void ToggleGrid()
    {
        PanoramicManager.instance.ToggleGrid();
    }

    public void DeleteDownloads()
    {
        AssetLoader.instance.DeleteDownloads();
    }


    public void ApplyReload()
    {
        DissableAll();
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainScene");
    }

    public void QuitApplication()
    {
        DissableAll();
        Application.Quit();
    }

    void OnEnable()
    {
        DissableAll();
    }

}
