﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class selected : MonoBehaviour {

    private Color selectedColor = new Color(100, 100, 100, 1);
    private Color unSelectedColor = new Color(255, 255, 255, 1);

    public void MenuSelected()
    {
        gameObject.GetComponent<Image>().color = Color.gray;
    }

    public void MenuUnSelected()
    {
        gameObject.GetComponent<Image>().color = Color.white;
    }
}
