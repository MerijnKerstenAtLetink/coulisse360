﻿using UnityEngine;
using System.Collections;
using TreeNutsGames.Trace;

namespace LetinkDesign.VR
{
    public class VRButtonWeb : VRButton
    {
        public GameObject actionObject;
        public string actionName;
        public string url;

        public void ActivateVRButton()
        {
           // Trace.Log("Action = " + actionName);

            actionObject.SendMessage(actionName, url);   
        }
    }
}

