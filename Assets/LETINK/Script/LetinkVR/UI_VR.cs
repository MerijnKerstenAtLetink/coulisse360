﻿using UnityEngine;
using System.Collections;

public class UI_VR : MonoBehaviour {

    [SerializeField]
    private Transform trackTransform;
    [SerializeField]
    private RectTransform rt;

    private float rotY;
    [SerializeField]
    private float followSpeed = 1f;

    void Start ()
    {
	
	}
	
	
	void Update ()
    {

        if(rt != null && trackTransform != null)
        {
            rotY = Mathf.LerpAngle(rt.localEulerAngles.y, trackTransform.localEulerAngles.y, Time.deltaTime * followSpeed);
            rt.localEulerAngles = new Vector3(0f, rotY, 0f);
        }
        
	}

    public void SetFollowSpeed(float newSpeed)
    {
        followSpeed = newSpeed;
    }
}
