﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace LetinkDesign.VR
{
    public class VRButton : MonoBehaviour
    {

        #region Properties

        [SerializeField]
        private float gazeTimer = 0f;

        [SerializeField]
        private float targetTime = 2f;

        [SerializeField]
        private float OnActivatedTimeOut = 3f;

        [SerializeField]
        private Image progressIndicator;

        private Coroutine haltRoutine;
        private Coroutine timeOutRoutine;
        [SerializeField]
        private bool buttonEnabled = true;

        private bool gazeUpon = false;


        [SerializeField]
        private EffectItem[] effectItems;

        #endregion



        #region Init

        void OnEnable()
        {
            Init();
        }


        private void Init()
        {
            gazeTimer = 0f;
            buttonEnabled = true;
            UpdateIndicator(gazeTimer);
        }

        #endregion



        #region Gazing and Releasing


        /// <summary>
        /// This button is gazed uppon by (adapted) GazeInputModule 
        /// </summary>
        public void HoldGaze()
        {
            gazeUpon = true;

            if (buttonEnabled)
            {
                gazeTimer += Time.deltaTime;
                if (gazeTimer >= targetTime)
                {
                   // Debug.Log("Triggered button " + gameObject.name);
                    VRButtonTriggered();
                }
                UpdateIndicator(gazeTimer);
                UpdateEffects();
            }
        }

        private void UpdateEffects()
        {
            foreach (EffectItem item in effectItems)
            {
                if(item != null)
                {
                    item.UpdateEffect();
                }
            }
        }



        /// <summary>
        /// Gazing focus is lost and is halting
        /// </summary>
        public void LostGaze()
        {
            gazeUpon = false;

            if (haltRoutine == null && gameObject.activeInHierarchy == true)
            {
                haltRoutine = StartCoroutine(HaltGaze());
            }
        }

        
        private IEnumerator HaltGaze()
        {
            //Debug.Log("HaltGaze");
            while (gazeTimer > 0f)
            {
                gazeTimer -= Time.deltaTime / 4f;
                UpdateIndicator(gazeTimer);
                yield return null;
            }
            if (gazeTimer <= 0f)
            {
                gazeTimer = 0f;
                if (haltRoutine != null)
                {
                    StopCoroutine(haltRoutine);
                    haltRoutine = null;
                }
                UpdateIndicator(gazeTimer);
            }
        }



        private void UpdateIndicator(float gazeTimer)
        {
            if (progressIndicator != null)
            {
                float value = (1f / targetTime * gazeTimer);
                progressIndicator.fillAmount = value;
            }
        }

        #endregion



        #region Button triggered and Ending

        private void VRButtonTriggered()
        {
            buttonEnabled = false;
            gazeTimer = 0f;
            if (GetComponent<AreaSwitchButton>())
            {
                GetComponent<AreaSwitchButton>().ActivateVRButton();
            }
            else if(GetComponent<VRButtonAction>())
            {
                VRButtonAction vrAction = GetComponent<VRButtonAction>();
                vrAction.ActivateVRButton();
            }
            else if (GetComponent<VRButtonWeb>())
            {
                VRButtonWeb vrAction = GetComponent<VRButtonWeb>();
                vrAction.ActivateVRButton();
            }

            if (gameObject.activeInHierarchy)
            {
                timeOutRoutine = StartCoroutine(TimeOutButton());
            }
        }



        private IEnumerator TimeOutButton()
        {
            yield return new WaitForSeconds(OnActivatedTimeOut);
            buttonEnabled = true;
        }
        

        void OnDisable()
        {
            if (timeOutRoutine != null)
            {
                StopCoroutine(timeOutRoutine);
                timeOutRoutine = null;
            }

            if (haltRoutine != null)
            {
                StopCoroutine(haltRoutine);
                haltRoutine = null;
            }
        }

        #endregion

    }
}