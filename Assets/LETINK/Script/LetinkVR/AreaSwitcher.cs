﻿using UnityEngine;
using System.Collections;


namespace LetinkDesign.VR
{
    public class AreaSwitcher : MonoBehaviour
    {
        public void SwitchToArea(string areaName)
        {
            SceneManager.instance.ToScene(areaName);
        }
    }
}
