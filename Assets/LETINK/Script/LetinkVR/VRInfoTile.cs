﻿using UnityEngine;
using System.Collections;
using LetinkDesign.VR;
using LetinkDesign.UI;
using LetinkDesign;
using TreeNutsGames.Trace;
using System;
using UnityEngine.UI;

public class VRInfoTile : MonoBehaviour
{
    private bool active = false;
    public float speed = 1f;

    [SerializeField]
    private Transform infoTileObject;
    private Vector3 originalPosition;
    private Quaternion originalRotation;
    private Vector3 maxScale = Vector3.one;
    private Transform originalParent;
    private RectTransform rectTransform;
    private Vector3 originalScale;

    [SerializeField]
    private Image triggerInfoSprite;

    [SerializeField]
    private Sprite openInfoSprite;

    [SerializeField]
    private Sprite closeInfoSprite;


    void Start()
    {
        VrModeManager.instance.VrModeManagerOnSwitch += this.OnVrModeSwitch;
        SceneManager.instance.StartSceneSwitch += this.OnStartSceneSwitch;

        rectTransform = GetComponent<RectTransform>();
        GetOrigins();
        infoTileObject.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    }

    public void OnVrModeSwitch(VrModeManager vrModeManager, EventArgs e)
    {
        this.enabled = true;
        if (active && !SceneManager.instance.isSwitching)
        {
            if (VrModeManager.instance.vrMode == VRMode.Single)
            {
                GoUiMode();
                UI_Manager.uiMode = true;
            }
            else
            {
                UnGoUiMode();
            }
        }
    }


    public void OnStartSceneSwitch(SceneManager sceneManager, EventArgs e)
    {
        if (true)
        {
            if (VrModeManager.instance.vrMode == VRMode.Single)
            {
                //Trace.Log("Tile " + transform.name + " return to pos");
                UnGoUiMode();
                UI_Manager.uiMode = false;
            }
        }
    }



    private void GetOrigins()
    {
        originalPosition = rectTransform.localPosition;
        originalRotation = rectTransform.localRotation;
        originalParent = transform.parent;
        originalScale = infoTileObject.transform.localScale;
    }

    public void ShowInfo()
    {
        if(!active)
        {
            active = true;
            
            if(VrModeManager.instance.vrMode == VRMode.Single)
            {
                GoUiMode();
                UI_Manager.uiMode = true;
            }
            
        }
        else
        {
            active = false;
            UI_Manager.uiMode = false;

        }
        this.enabled = true;
    }

    private void GoUiMode()
    {

        maxScale = new Vector3(0.8f, 0.8f, 0.8f);
        transform.parent = UI_Manager.uiMainCanvas;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;
        rectTransform.localPosition = Vector3.zero; // ?
        rectTransform.anchoredPosition = new Vector3(300f, -200, 0f);
        transform.SetAsFirstSibling();
    }

   

    private void UnGoUiMode()
    {
        transform.parent = originalParent;
        maxScale = Vector3.one;

        transform.localPosition = Vector3.zero; // ?
        rectTransform.localPosition = originalPosition;
        rectTransform.localRotation = originalRotation;
        
        transform.localScale = originalScale;
        this.enabled = false;
    }


    void Update()
    {
        if(active)
        {
            infoTileObject.localScale = Vector3.Lerp(infoTileObject.localScale, maxScale, Time.deltaTime * speed);
            if (Vector3.Distance(infoTileObject.localScale, maxScale) < 0.01f)
            {
                infoTileObject.localScale = maxScale;
                if (triggerInfoSprite != null && closeInfoSprite != null)
                {
                    triggerInfoSprite.sprite = closeInfoSprite;
                }
                else
                {
                    Debug.Log(transform.name + " is not using spriteswap");
                }
                this.enabled = false;
            }
        }
        else
        {
            infoTileObject.localScale = Vector3.Lerp(infoTileObject.localScale, Vector3.zero, Time.deltaTime * speed);
            if (Vector3.Distance(infoTileObject.localScale, Vector3.zero) < 0.01f)
            {
                infoTileObject.localScale = Vector3.zero;
                if(triggerInfoSprite != null && openInfoSprite != null)
                {
                    triggerInfoSprite.sprite = openInfoSprite;
                }
                else
                {
                    Debug.Log(transform.name + " is not using spriteswap");
                }
                UnGoUiMode();
                this.enabled = false;
            }
        }
    }

    public void WebSiteRequest(string url)
    {
        if (VrModeManager.instance.vrMode == VRMode.Single)
        {
            Application.OpenURL(url);
        }
        else
        {
            GameMain.QueUrl(url);
        }
    }

    public void CloseTile()
    {
        active = false;
        this.enabled = true;
    }
}
