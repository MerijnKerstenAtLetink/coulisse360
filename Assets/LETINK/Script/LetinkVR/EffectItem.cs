﻿using UnityEngine;

namespace LetinkDesign.VR
{
    public class EffectItem : MonoBehaviour
    {
        public bool effectActive = false;

        public void UpdateEffect()
        {
            effectActive = true;
            this.enabled = true;
        }
    }
}