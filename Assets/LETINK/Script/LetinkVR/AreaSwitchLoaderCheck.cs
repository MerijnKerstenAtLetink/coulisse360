﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using LetinkDesign;

using System;
using TreeNutsGames.Trace;

namespace LetinkDesign.VR
{

    public class AreaSwitchLoaderCheck : MonoBehaviour
    {

        #region Properties

        public FileState myState = FileState.Unknown;

        public Image statusTile;

        public string areaName;
        public string assetID = "";

        [SerializeField]
        private Sprite offline;

        [SerializeField]
        private Sprite notReady;

        [SerializeField]
        private Sprite online;

        [SerializeField]
        private Sprite downloading;

        [SerializeField]
        private Sprite timeOut;

        [SerializeField]
        private Sprite ready;

        [SerializeField]
        private Sprite fileError;

        private bool done = false;
        private bool checking = false;

        private Coroutine checkRoutine;

        [SerializeField]
        private float serverRefreshRate = 4f;

        private bool failedDownload = false;


        [SerializeField]
        private Image downloadProgressIndicator;

        [SerializeField]
        private bool invertProgressIndicator = false;

        //amount of server connect tries before sleeping..  this is order to save on energy and data consumption
        [SerializeField]
        private int serverTrySleepCount = -1;
        private int resetCount = 3;

        [SerializeField]
        private Text messageText;
        private Image messageDisplay;


        #endregion Properties



        #region Init


        void Awake()
        {
            myState = FileState.Unknown; // offline
            resetCount = serverTrySleepCount;
            messageDisplay = messageText.transform.parent.GetComponent<Image>();
            if (downloadProgressIndicator)
            {
                downloadProgressIndicator.fillAmount = invertProgressIndicator ? 0f : 1f;
            }
        }




        private IEnumerator GetAssetID()
        {
            while (SceneManager.instance == null)
            {
             //   Debug.Log("waiting for scene manager to be ready !");
                yield return null;
            }

            while (AssetLoader.instance.assetLoaderReady == false)
            {
           //     Debug.Log("waiting for the assetLoader to be ready !");
                yield return null;
            }

            DisableInterAct();

            UnityEngine.Random rd = new UnityEngine.Random();
            float disp = UnityEngine.Random.Range(0.5f, 5f);
            yield return new WaitForSeconds(disp);

            areaName = GetComponent<AreaSwitchButton>().areaName;
            areaName = areaName.ToLower();
            assetID = SceneManager.instance.AssetID(areaName); //  ask if assetID is present

            yield return null;

            checkRoutine = StartCoroutine(CheckState());

            EnableInterAct();

            yield return null;
        }


        /// <summary>
        /// Loops and waits until the assetloader returns the correct item
        /// </summary>
        /// <returns></returns>
        private IEnumerator CheckState()
        {

            Init();

            if (myState == FileState.Ready)
            {
                checkRoutine = null;
         //       Debug.Log("CheckRoutine ready and ended");
                TileMessage("Ready to play", -1f);
                if (downloadProgressIndicator)
                {
                    downloadProgressIndicator.fillAmount = invertProgressIndicator ? 1f : 0f;
                }
            }
            else if (myState == FileState.Unknown || myState == FileState.NotReady)
            {
                StartCoroutine(RedoCheck());
                TileMessage("Searching", -1f);
                Debug.Log("LoopCheck wait");
            }
            else if (myState == FileState.Online)
            {
                StartCoroutine(RedoCheck());
                Debug.Log("LoopCheck wait");
            }
            else if (myState == FileState.Downloading)
            {
                StartCoroutine(RedoDownloadWait());
                statusTile.sprite = downloading;
                Debug.Log("Download wait");
            }
            else if (myState == FileState.Failed)
            {
                checkRoutine = null;
                Debug.Log("CheckRoutine failed and ended");
            }

            yield return new WaitForEndOfFrame();

        }


        private IEnumerator RedoCheck()
        {
            yield return new WaitForSeconds(serverRefreshRate);

            StopCoroutine(CheckState());

            checkRoutine = null;
            if (serverTrySleepCount > 0)
            {
                serverTrySleepCount--;
            }

            if (serverTrySleepCount == 0)
            {

            }
            else
            {
                checkRoutine = StartCoroutine(CheckState());
            }

        }


        private IEnumerator RedoDownloadWait()
        {
            myState = FileState.Downloading;
            yield return new WaitForSeconds(serverRefreshRate);
            checkRoutine = null;
            checkRoutine = StartCoroutine(CheckState());
        }


        private void Init()
        {
            bool fileIsLocal = false;


            TileMessage("Offline", -1f);
            statusTile.sprite = offline;


            if (assetID != "")
            {
                fileIsLocal = AssetLoader.instance.GetFileByName(assetID);
            }
            else
            {
                Debug.Log("???");
                myState = FileState.Unknown;
                TileMessage("Unknown", -1f);
                return;
            }


            if (fileIsLocal == true)
            {
                statusTile.sprite = ready;
                myState = FileState.Ready;
                TileMessage("Ready to play", -1f);
                EnableInterAct();
                Resources.UnloadUnusedAssets();
            }
            else if (AssetLoader.instance.serverIsOnline && myState != FileState.Downloading)
            {
                statusTile.sprite = online;
                myState = FileState.Online;
                serverTrySleepCount = 0;
                TileMessage("Online", -1f);
                EnableInterAct();
            }
            else if (myState == FileState.Downloading)
            {
                statusTile.sprite = downloading;
                TileMessage("Downloading", -1f);

            }
            else if (myState == FileState.Failed)
            {
                statusTile.sprite = fileError;
                TileMessage("Dowload failed", -1f);
                EnableInterAct();
            }
            else
            {
                statusTile.sprite = offline;
                myState = FileState.Unknown;
                TileMessage("Offline", -1f);
            }
        }


        #endregion Init



        #region Asset checking and Loading


        internal void LoadAsset()
        {
            Resources.UnloadUnusedAssets();

            if (myState != FileState.Downloading || myState != FileState.Ready)
            {

                if (!string.IsNullOrEmpty(assetID))
                {
                    TileMessage("Downloading", -1f);
                    myState = FileState.Downloading;
                    statusTile.sprite = downloading;
                    checkRoutine = StartCoroutine(Download());
                    DisableInterAct();
                }
                else
                {
                    TileMessage("Can't find file", -1f);
                    Debug.Log("Can't load asset");
                    EnableInterAct();
                }
            }
        }

        private IEnumerator Download()
        {

            if (AssetLoader.instance.isDownloading == false)
            {
                AssetLoader.instance.Download(assetID, this);
                StartCoroutine(RedoDownloadWait());
            }
            else
            {
                TileMessage("Busy...", -1f);
                if (checkRoutine != null)
                {
                    StopCoroutine(checkRoutine);
                    checkRoutine = null;
                }

                StartCoroutine(TimeOut());
            }

            yield return new WaitForEndOfFrame();
        }

        internal void UpdateProgress(float progress)
        {
            //Debug.Log("Download @ " + progress * 100 + " %");
            myState = FileState.Downloading;
            statusTile.sprite = downloading;
            TileMessage("Downloading", 0f);
            if (downloadProgressIndicator != null)
            {
                if (invertProgressIndicator)
                {
                    downloadProgressIndicator.fillAmount = progress;
                }
                else
                {
                    downloadProgressIndicator.fillAmount = 1f - progress;
                }
            }
        }

        private void TileMessage(string message, float showTime)
        {
            StartCoroutine(Message(message, showTime));
        }

        private IEnumerator Message(string message, float showTime)
        {
            if (messageText != null)
            {
                messageText.text = message;
                messageDisplay.enabled = true;

                if (showTime > 0f)
                {
                    yield return new WaitForSeconds(showTime);
                    messageText.text = "";
                    messageDisplay.enabled = false;
                }
            }
            yield return null;
        }

        public void FailedDownload()
        {
            myState = FileState.Failed;
            Debug.Log("FailedDownLoad");
            EnableInterAct();
        }

        private void EnableInterAct()
        {
            GetComponent<Image>().raycastTarget = true;
        }

        private void DisableInterAct()
        {
            GetComponent<Image>().raycastTarget = false;
            //Trace.Log("Should disable " + transform.name);
        }

        private IEnumerator TimeOut()
        {
            statusTile.sprite = timeOut;
            yield return new WaitForSeconds(5f);
            Reset();
        }



        public void Reset()
        {
            serverTrySleepCount = resetCount;
            myState = FileState.NotReady;
            if (AssetLoader.instance.serverIsOnline == false)
            {
                AssetLoader.instance.Init();
            }
            checkRoutine = null;
            checkRoutine = StartCoroutine(CheckState());
            EnableInterAct();
            Init();
            this.enabled = true;
        }

        void OnDestroy()
        {
            if (checkRoutine != null)
            {
                StopCoroutine(checkRoutine);
                checkRoutine = null;
            }
            StopAllCoroutines();
        }

        void OnEnable()
        {
            switch (myState)
            {
                case FileState.Unknown:
                    StartCoroutine(GetAssetID());
                    break;
                case FileState.NotReady:
                    StartCoroutine(GetAssetID());
                    Reset();
                    break;
                case FileState.Online:
                    StartCoroutine(GetAssetID());
                    Reset();
                    break;
                case FileState.Downloading:
                    Reset();
                    break;
                case FileState.Ready:
                    Reset();
                    break;
                case FileState.Failed:
                    StartCoroutine(GetAssetID());
                    Reset();
                    break;
                default:
                    break;
            }

        }

        void OnDisable()
        {
            StopAllCoroutines();
        }

        #endregion Asset checking and Loading
    }
}