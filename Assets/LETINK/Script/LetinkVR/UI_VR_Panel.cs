﻿using UnityEngine;
//using UnityEditor;
using System.Collections;
using LetinkDesign.VR;
using LetinkDesign.UI;
using TreeNutsGames.Trace;
using LetinkDesign;
using System;

public class UI_VR_Panel : MonoBehaviour
{
    [SerializeField]
    private UI_VR tracker;

    [SerializeField]
    private GameObject vrPanel;
    
    private bool panelActive;
    
    [SerializeField]
    private Vector3 targetInPostion;

    [SerializeField]
    private Vector3 targetOutPosition;

    [SerializeField]
    private float travelSpeed = 1f;

    [SerializeField]
    private MediaPlayerCtrl videoPlayer;

    void Start()
    {
        DisablePanels();
    }


    #region menuFunctions

    public void ActivateVRSettings()
    {
        VRPanel();
        if(!panelActive)
        {
            panelActive = true;
            StartCoroutine(TravelIn());
        }
        else
        {
            StartCoroutine(TravelOut());
        }        
    }

    public void CloseVRSettings()
    {
        StartCoroutine(TravelOut());
    }

    private IEnumerator TravelIn()
    {
        while (Vector3.Distance(transform.position,targetInPostion) > 0.02f)
        {
            transform.position = Vector3.Lerp(transform.position, targetInPostion, Time.deltaTime * travelSpeed);
            yield return null;
        }
    }

    private IEnumerator TravelOut()
    {
        while (Vector3.Distance(transform.position, targetOutPosition) > 0.02f)
        {
            transform.position = Vector3.Lerp(transform.position, targetOutPosition, Time.deltaTime * travelSpeed);
            yield return null;
        }
        panelActive = false;
        DisablePanels();
    }

    public void VRPanel()
    {
        DisablePanels();
        vrPanel.SetActive(true);
        tracker.SetFollowSpeed(0f);
    }

    #endregion menuFunctions

    #region functionality

    public void Home()
    {
        CloseVRSettings();
        SceneManager.instance.ToScene("Home");
    }

    public void Map()
    {
        CloseVRSettings();
        SceneManager.instance.ToScene("EnterPortal");
    }

    public void SetVrMode(bool vr)
    {
        VrModeManager.instance.SetVRMode(vr, true);
        Settings.instance.playerSettings.playerVrMode = vr;
    }


    public void NonVrMode()
    {
        VrModeManager.instance.SetVRMode(false, true);
        DisablePanels();
        vrPanel.transform.parent.gameObject.SetActive(false);
    }
    
    public void Recenter()
    {
        Cardboard.SDK.Recenter();
    }
    
    // Please note that for correct culling on LensCorrection switch:
    // On the Left eye:  Cardboard Eye script needs ToggleCulling mask set to Nothing
    // On the Right eye:  Cardboard Eye script needs ToggleCulling mask set to Left eye and Right eye

    public void LensCorrection()
    {
        Cardboard.SDK.DistortionCorrection = Cardboard.DistortionCorrectionMethod.Unity;
        Settings.instance.playerSettings.useLensDistortionCorrection = true;
        VrModeManager.instance.InitVRLive();
    }

    public void NoLensCorrection()
    {
        Cardboard.SDK.DistortionCorrection = Cardboard.DistortionCorrectionMethod.None;
        Settings.instance.playerSettings.useLensDistortionCorrection = false;
        VrModeManager.instance.InitVRLive();
    }

    public void QualityLow()
    {
        Settings.instance.playerSettings.videoQuality = Settings.VideoQuality.Low;
    }

    public void QualityMedium()
    {
        Settings.instance.playerSettings.videoQuality = Settings.VideoQuality.Medium;
    }

    public void QualityHigh()
    {
        Settings.instance.playerSettings.videoQuality = Settings.VideoQuality.High;
    }

    public void ReloadNow()
    {
        StartCoroutine(ReloadWithFader());
    }

    private IEnumerator ReloadWithFader()
    {
        UIFader.instance.StartFadeOut(1f);
        yield return new WaitForSeconds(1f);
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainScene");
    }

    private void DisablePanels()
    {
        vrPanel.SetActive(false);
    }

    public void QuitApp()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }

    public void ToggleSound()
    {
        if (videoPlayer.m_fVolume != 0f)
        {
            videoPlayer.SetVolume(0f);
        }
        else
        {
            videoPlayer.SetVolume(0.5f);
        }
     }

    public void LowerVolume()
    {
        if (videoPlayer.m_fVolume < 0.9f)
        {
            videoPlayer.m_fVolume += 0.1f;
            videoPlayer.SetVolume(videoPlayer.m_fVolume);
        }
        
    }

    public void HigherVolume()
    {
        videoPlayer.m_fVolume -= 0.1f;
        videoPlayer.SetVolume(videoPlayer.m_fVolume);
    }

    public void OpenSub(GameObject menu)
    {
        menu.SetActive(true);
    }

    public void CloseSub(GameObject menu)
    {
        menu.SetActive(false);
    }

    #endregion functionality

}