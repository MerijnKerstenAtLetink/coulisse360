﻿using UnityEngine;
using System.Collections;
using LetinkDesign.VR;


namespace LetinkDesing.VR
{
    public class ItemMove : EffectItem
    {

        [SerializeField]
        private Vector3 startPoint;

        [SerializeField]
        private Vector3 endPoint;
        [SerializeField]
        private float speed;




        void Start()
        {
            if(startPoint == Vector3.zero)
            {
                startPoint = transform.localPosition;
            }
        }

        void Update()
        {
            if(effectActive)
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, endPoint, speed * Time.deltaTime);
            }
            else
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, startPoint, speed * Time.deltaTime);
                if (Vector3.Distance(startPoint , endPoint) < 0.0001f)
                {
                    this.enabled = false;
                }
            }
            effectActive = false;
        }

        [ContextMenu("ShowEndPoint")]
        public void ShowEndPoint()
        {
            transform.localPosition = endPoint;
        }


        [ContextMenu("ShowStartPoint")]
        public void ShowStartPoint()
        {
            transform.localPosition = startPoint;
        }



        [ContextMenu("SetEndPoint")]
        public void SetEndPoint()
        {
            endPoint = transform.localPosition;
        }


        [ContextMenu("SetStarPoint")]
        public void SetStarPoint()
        {
            startPoint = transform.localPosition;
        }
    }
}

