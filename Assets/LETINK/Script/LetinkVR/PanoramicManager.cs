﻿using UnityEngine;
using System.Collections;
using TreeNutsGames.Trace;
using System;

namespace LetinkDesign.VR
{
    public class PanoramicManager : MonoBehaviour
    {

        #region Properties

        public static PanoramicManager instance;

        public Transform leftSphere;
        public Transform rightSphere;
        private Transform leftCamTransform;
        private Transform rightCamTransform;

        private bool stereoMode = false;

        [SerializeField]
        private Material monoFull;

        [SerializeField]
        private Material stereoLeft;

        [SerializeField]
        private Material stereoRight;

        [SerializeField]
        private Material videoMaterial;


        [SerializeField]
        private Mesh monoFullMesh;

        [SerializeField]
        private Mesh stereoTopDownLeft;

        [SerializeField]
        private Mesh stereoTopDownRight;

        [SerializeField]
        private Mesh stereoSideBySideLeft;

        [SerializeField]
        private Mesh stereoSideBySideRight;

        public MediaPlayerCtrl videoPlayer;

        //public string videoPath;
        [SerializeField]
        private Texture2D defaultTexture;

        private bool updateSpheres;

        [SerializeField]
        private GameObject gridSphere;

        [SerializeField]
        private Transform camPivot;

        [SerializeField]
        private Transform threeDRelativeRotationTransform;



        #endregion Properties



        #region Init

        private void Awake()
        {
            instance = this;
            if (leftSphere == null || rightSphere == null)
            {
                Debug.LogError("PanoramicManager is not set up properly");
            }
        }


        #endregion Init



        #region Panoramic switching


        public void InitPanoramic(SceneManager.Scene scene)
        {
            if (scene.sceneType == SceneType.VideoMono || scene.sceneType == SceneType.VideoStereoTopDown || scene.sceneType == SceneType.VideoStereoSideBySide)
            {
                if (GetComponent<MediaPlayerCtrl>())
                {
                    videoPlayer = GetComponent<MediaPlayerCtrl>();
                }
            }
            StartCoroutine(InitSceneType(scene));
        }


        private IEnumerator InitSceneType(SceneManager.Scene scene)
        {
        //    Debug.Log("nextScene is a " + scene.sceneType.ToString());
            Renderer leftRenderer = leftSphere.gameObject.GetComponent<Renderer>();
            Renderer rightRenderer = rightSphere.gameObject.GetComponent<Renderer>();
            MeshFilter leftMesh = leftSphere.GetComponent<MeshFilter>();
            MeshFilter rightMesh = rightSphere.GetComponent<MeshFilter>();

            StopVideo();

        //    Debug.Log("PanoramicManager to " + scene.sceneType.ToString());

            yield return null;

            switch (scene.sceneType)
            {
                case SceneType.Panoramic:
                    leftSphere.gameObject.SetActive(true);
                    rightSphere.gameObject.SetActive(true);
                    leftMesh.mesh = monoFullMesh;
                    rightMesh.mesh = monoFullMesh;
                    leftRenderer.material = monoFull;
                    rightRenderer.material = monoFull;



                    break;
                case SceneType.PanoramicStereoDualMono:
                    leftSphere.gameObject.SetActive(true);
                    rightSphere.gameObject.SetActive(true);
                    leftMesh.mesh = monoFullMesh;
                    rightMesh.mesh = monoFullMesh;
                    leftRenderer.material = stereoLeft;
                    rightRenderer.material = stereoRight;



                    break;
                case SceneType.PanoramicStereoSideBySide:
                    leftSphere.gameObject.SetActive(true);
                    rightSphere.gameObject.SetActive(true);
                    leftMesh.mesh = stereoSideBySideLeft;
                    rightMesh.mesh = stereoSideBySideRight;
                    leftRenderer.material = monoFull;
                    rightRenderer.material = monoFull;



                    break;
                case SceneType.PanoramicStereoTopDown:
                    leftSphere.gameObject.SetActive(true);
                    rightSphere.gameObject.SetActive(true);
                    leftMesh.mesh = stereoTopDownLeft;
                    rightMesh.mesh = stereoTopDownRight;
                    leftRenderer.material = monoFull;
                    rightRenderer.material = monoFull;

                    MirrorSpheres();

                    break;
                case SceneType.VideoMono:
                    leftSphere.gameObject.SetActive(true);
                    rightSphere.gameObject.SetActive(true);
                    leftMesh.mesh = monoFullMesh;
                    rightMesh.mesh = monoFullMesh;
                    leftRenderer.material = videoMaterial;
                    rightRenderer.material = videoMaterial;

                    yield return null;

                    MirrorSpheres();

                    break;
                case SceneType.VideoStereoSideBySide:
                    leftSphere.gameObject.SetActive(true);
                    rightSphere.gameObject.SetActive(true);
                    leftMesh.mesh = stereoSideBySideLeft;
                    rightMesh.mesh = stereoSideBySideRight;
                    leftRenderer.material = videoMaterial;
                    rightRenderer.material = videoMaterial;

                    yield return null;

                    MirrorSpheres();

                    break;
                case SceneType.VideoStereoTopDown:
                    leftSphere.gameObject.SetActive(true);
                    rightSphere.gameObject.SetActive(true);
                    leftMesh.mesh = stereoTopDownLeft;
                    rightMesh.mesh = stereoTopDownRight;
                    leftRenderer.material = stereoLeft;
                    rightRenderer.material = stereoRight;

                    yield return null;

                    MirrorSpheres();

                    break;
                case SceneType.ThreeD:                  // Left eye : Layer left && default  + Right eye : Layer right && default
                    leftRenderer.material = null;
                    rightRenderer.material = null;
                    leftSphere.gameObject.SetActive(false);
                    rightSphere.gameObject.SetActive(false);
                    break;
                default:
                    break;
            }





            stereoMode = scene.adjustSpheres;
            SetSphereStereoMode(stereoMode);
            yield return null;

            AdjustSceneRotation(scene);
            ActivateGrid(Settings.instance.playerSettings.developerMode && Settings.instance.playerSettings.showGrid);

        }

        internal void ToggleGrid()
        {
            gridSphere.SetActive(!gridSphere.activeInHierarchy);
            Settings.instance.playerSettings.showGrid = gridSphere.activeInHierarchy;
        }

        public void ActivateGrid(bool grid)
        {
            gridSphere.SetActive(grid);
        }

        private void AdjustSceneRotation(SceneManager.Scene nextScene)
        {
            if(nextScene.sceneType == SceneType.ThreeD)
            {
                AdjustThreeDSceneRotation(nextScene);
            }
            else
            {
                AdjustPanoramicRotation(nextScene);
            }
           
        }


        private void AdjustPanoramicRotation(SceneManager.Scene nextScene)
        {
            if (camPivot != null)
            {
                foreach (GameObject go in nextScene.sceneObjects)
                {
                    go.transform.localEulerAngles = Vector3.zero;
                    transform.localEulerAngles = Vector3.zero;

                    if (nextScene.forceStartRotationToCam)
                    {
                        go.transform.localEulerAngles = new Vector3(0f, camPivot.localEulerAngles.y, 0f);
                        transform.localEulerAngles = new Vector3(0f, camPivot.localEulerAngles.y, 0f);
                    }
                    
                    float addY = transform.localEulerAngles.y;
                    addY += nextScene.addYRotation;
                    go.transform.localEulerAngles = new Vector3(0f, addY, 0f);
                    transform.localEulerAngles = new Vector3(0f, addY, 0f);
                }
            }
            else
            {
                Debug.Log("Can't rotate the scene");
            }
        }


        private void AdjustThreeDSceneRotation(SceneManager.Scene nextScene)
        {
            if (camPivot != null && threeDRelativeRotationTransform != null)
            {
                threeDRelativeRotationTransform.eulerAngles = Vector3.zero;
                threeDRelativeRotationTransform.position = camPivot.parent.position;

                foreach (GameObject go in nextScene.sceneObjects)
                {

                    Transform formerParent = go.transform.parent;

                    go.transform.parent = threeDRelativeRotationTransform;

                    if (nextScene.forceStartRotationToCam)
                    {
                        threeDRelativeRotationTransform.localEulerAngles = new Vector3(0f, camPivot.eulerAngles.y, 0f);
                    }

                    float addY = threeDRelativeRotationTransform.localEulerAngles.y;
                    addY += nextScene.addYRotation;
                    threeDRelativeRotationTransform.localEulerAngles = new Vector3(0f, addY, 0f);

                    go.transform.parent = formerParent;
                }
            }
            else
            {
                Debug.Log("Can't rotate the scene");
            }
        }

        

        public void SetSphereStereoMode(bool stereo)
        {
            stereoMode = stereo;
            if (stereo)
            {
                leftCamTransform = VrModeManager.instance.leftEyeCams[0].transform;
                rightCamTransform = VrModeManager.instance.rightEyeCams[0].transform;
                if (leftSphere != null && rightSphere != null)
                {
                    if (leftCamTransform != null && rightCamTransform != null)
                    {
                        updateSpheres = true;
                    }
                    else
                    {
                        updateSpheres = false;
                        Debug.Log("PanoramicManager: cam not connected");
                    }
                }
                else
                {
                    updateSpheres = false;
                    Debug.Log("PanoramicManager: spheres not connected");
                }
            }
            else
            {
                updateSpheres = false;
                leftCamTransform = null;
                rightCamTransform = null;
                if (leftSphere != null && rightSphere != null)
                {
                    leftSphere.localPosition = Vector3.zero;
                    rightSphere.localPosition = Vector3.zero;
                }
                else
                {
                    Debug.LogError("PanoramicManager: Spheres not found");
                }
            }
        }



        private void MirrorSpheres()
        {

#if UNITY_IOS || UNITY_TVOS || UNITY_EDITOR || UNITY_STANDALONE

            //   Trace.Log("IOS Set Reverse");

            if (leftSphere != null)
                {
                    SphereMirror.MirrorSpheres(leftSphere);
                }

                if (rightSphere != null)
                {
                    SphereMirror.MirrorSpheres(rightSphere);
                }
#endif
        }

       



        #endregion Panoramic switching



        #region Update Spheres


        /// <summary>
        /// Call this every frame to update the positions of the panoramic
        /// spheres to adjust for the stereo cameras ipd versus position
        /// for even better stereo from 360 panoramics. Typically should be called from
        /// LateUpdate and nicely optimized if called from GameMain Update()
        /// </summary>
        internal void UpdateSpheres()
        {
            if(updateSpheres)
            {
                if (leftSphere != null && leftCamTransform != null)
                {
                    leftSphere.position = leftCamTransform.position;
                    rightSphere.position = rightCamTransform.position;
                }
            }
        }


        #endregion Update Spheres



        #region Panoramic textures


        public void LeftPano(Texture2D leftTex)
        {
            monoFull.mainTexture = leftTex;
            stereoLeft.mainTexture = leftTex;
        }

        public void RightPano(Texture2D rightTex)
        {
            stereoRight.mainTexture = rightTex;
        }

        public void Clear()
        {
            stereoLeft.mainTexture = null;
            stereoRight.mainTexture = null;
            videoMaterial.mainTexture = null;
            Resources.UnloadUnusedAssets();
            videoMaterial.mainTexture = defaultTexture;
        }

        #endregion Panoramic textures



        #region Video functions


        internal IEnumerator RunVideo()
        {
            yield return null;
            if (videoPlayer != null)
            {
                //Debug.Log("RunVideo : " + videoPath);
                //videoPlayer.m_bInit = true;
                
                videoPlayer.Play();
            }
            else
            {
                Debug.Log("there is no video player attached !");
            }
        }

        internal void StopVideo()
        {
            if(videoPlayer != null)
            {
                //Trace.Log("StopVideo");
                videoPlayer.Stop();
                videoPlayer.UnLoad();
            }
            else
            {
             //   Debug.Log("there is no video player attached !");
            }
        }

        #endregion Video functions

    }
}

