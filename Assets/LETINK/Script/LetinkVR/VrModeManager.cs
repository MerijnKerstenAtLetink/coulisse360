﻿using LetinkDesing;
using System;
using TreeNutsGames.Trace;
using UnityEngine;


namespace LetinkDesign.VR
{
    public class VrModeManager : MonoBehaviour
    {

        #region Properties


        public static VrModeManager instance;
        public VRMode vrMode = VRMode.VR;
        [SerializeField]
        private bool forcedSingleMode = false;

        public Camera[] leftEyeCams;
        public Camera[] rightEyeCams;

        private int leftEyeIndex;
        private int rightEyeIndex;

        

        [SerializeField]
        private GameObject[] SingleScreenObjects;
        [SerializeField]
        private GameObject[] DualScreenObjects;
        [SerializeField]
        private GameObject[] vrAbleItems;


        [SerializeField]
        private CameraTouchMouse camTouchMouse;
        [SerializeField]
        private GameObject pointer;

        public bool isMovingCam = false;


        #region Event Delegates

        // 1- Define a delegate
        // 2- Define an event based on that delegate
        // 3- Raise the event

        //1
        public delegate void VrModeOnSwitchHandler(VrModeManager vrModeManager, EventArgs e);  // (object sender, EventArgs e)

        //2
        public event VrModeOnSwitchHandler VrModeManagerOnSwitch;

        #endregion Even Delegates



        #endregion Properties






        #region Init


        void Awake()
        {
            instance = this;
#if !UNITY_EDITOR
            if(vrMode == VRMode.None)
            {
                vrMode = VRMode.Single;
            }
#endif
        }


        public void Init()
        {
            // Debug.Log(LayerMask.NameToLayer("TransparentFX")); == 1;
            leftEyeIndex = LayerMask.NameToLayer("Left eye");
            rightEyeIndex = LayerMask.NameToLayer("Right eye");
           // Debug.Log("left eye index = " + leftEyeIndex);
          //  Debug.Log("right eye index = " + rightEyeIndex);

            if(leftEyeIndex == -1)
            {
                Debug.LogError("Left eye layer not setup correctly. Please assign 'Left eye' layer");
            }
            if(rightEyeIndex == -1)
            {
                Debug.LogError("Right eye layer not setup correctly. Please assign 'Right eye' layer");
            }

            SetVRMode(Settings.instance.playerSettings.playerVrMode, true);

        }


#endregion Init



        #region VRMode switching


        /// <summary>
        /// Inits and apply the VR mode
        /// </summary>
        /// <param name="nextScene"></param>
        public void InitVR(SceneManager.Scene nextScene)
        {
            //Debug.Log("Init VRModeManager to "+ nextScene.sceneType.ToString());

            DisableCulling();

            OnVrModeSwitch();

            if (Settings.instance.playerSettings.vrCapable == false)
            {
                forcedSingleMode = true;
            }

            if (forcedSingleMode || nextScene.modeProperty == ModeProperty.SingleOnly)
            {
                SetSingleScreenMode();
            }
            else if(nextScene.modeProperty == ModeProperty.VROnly || nextScene.modeProperty == ModeProperty.SingleAndVR)
            {
                if(vrMode == VRMode.Single)
                {
                    SetSingleScreenMode();
                }
                else if (vrMode == VRMode.VR)
                {
                    SetDualScreenMode();

                    switch (nextScene.sceneType)
                    {
                        case SceneType.Panoramic:
                            if (nextScene.adjustSpheres)
                            {
                                CullingStereo();
                            }
                            else
                            {
                                CullingMono();
                            }
                            break;
                        case SceneType.PanoramicStereoDualMono:
                            CullingStereo();
                            break;
                        case SceneType.PanoramicStereoTopDown:
                            CullingStereo();
                            break;
                        case SceneType.PanoramicStereoSideBySide:
                            CullingStereo();
                            break;
                        case SceneType.VideoMono:
                            if(nextScene.adjustSpheres)
                            {
                                CullingStereo();
                            }
                            else
                            {
                                CullingMono();
                            }
                            break;
                        case SceneType.VideoStereoTopDown:
                            CullingStereo();
                            break;
                        case SceneType.VideoStereoSideBySide:
                            CullingStereo();
                            break;
                        case SceneType.ThreeD:
                            CullingMono();
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Debug.Log("No VR mode given exception");
                    SetSingleScreenMode();
                }
            }

            UseSwipeTracking(Settings.instance.playerSettings.swipeTrackingOnly && !Settings.instance.playerSettings.playerVrMode);
        }

        public void InitVRLive()
        {
            InitVR(SceneManager.instance.currentScene);
        }

        /// <summary>
        /// Sets Cardboard sdk to single screen
        /// </summary>
        private void SetSingleScreenMode()
        {
            Cardboard.SDK.VRModeEnabled = false;
           // Cardboard.SDK.BackButtonMode = Cardboard.BackButtonModes.On;
            pointer.SetActive(false);

            SetHeadTrackingActive(!Settings.instance.playerSettings.swipeTrackingOnly);

            foreach (var sso in SingleScreenObjects)
            {
                sso.SetActive(true);
            }

            foreach (var dso in DualScreenObjects)
            {
                dso.SetActive(false);
            }

            if(forcedSingleMode)
            {
                foreach (var fso in vrAbleItems)
                {
                    fso.SetActive(false);
                }
            }
        }


        /// <summary>
        /// Sets Cardboard sdk to dual screen
        /// </summary>
        private void SetDualScreenMode()
        {
            Cardboard.SDK.VRModeEnabled = true;
           // Cardboard.SDK.BackButtonMode = Cardboard.BackButtonModes.On;

            // enable disable distortion from settings flag
            Cardboard.SDK.DistortionCorrection = 
                Settings.instance.playerSettings.useLensDistortionCorrection == true ? 
                Cardboard.DistortionCorrectionMethod.Unity : 
                Cardboard.DistortionCorrectionMethod.None;
            //


            pointer.SetActive(true);

            SetHeadTrackingActive(true);

            foreach (var ds in DualScreenObjects)
            {
                ds.SetActive(true);
            }

            foreach (var ss in SingleScreenObjects)
            {
                ss.SetActive(false);
            }
        }



        /// <summary>
        /// Set VrModeManager to VR Mode
        /// </summary>
        /// <param name="vr"></param>
        /// <param name="now"></param>
        public void SetVRMode(bool vr, bool now)
        {
            vrMode = vr == true ? VRMode.VR : VRMode.Single;
            Settings.instance.playerSettings.playerVrMode = vr;
            if (now)
            {
                InitVR(SceneManager.instance.currentScene);
           //     Debug.Log("VRMode set [immediate] to " + vrMode.ToString());
            }
            else
            {
                Debug.Log("VRMode set [not immediate] to " + vrMode.ToString());
            }
            
            Settings.instance.Save();
        }


        public void UseSwipeTracking(bool swipeTrackingOnly)
        {
            SetHeadTrackingActive(!swipeTrackingOnly);
            camTouchMouse.TouchSwipeActive(swipeTrackingOnly);
            CamSmoother.instance.UseDamp(!swipeTrackingOnly && vrMode == VRMode.Single);
        }


        private static void SetHeadTrackingActive(bool headTracking)
        {
            CardboardHead.instance.trackPosition = headTracking;
            CardboardHead.instance.trackRotation = headTracking;
        }

        #endregion VRMode switching



        #region Camera Culling
        // Please note that for correct culling on LensCorrection switch:
        // On the Left eye:  Cardboard Eye script needs ToggleCulling mask set to Nothing
        // On the Right eye:  Cardboard Eye script needs ToggleCulling mask set to Left eye and Right eye


        /// <summary>
        /// Culls the stereo cams for stereo layering
        /// </summary>
        private void CullingStereo()
        {
            foreach (Camera lcam in leftEyeCams)
            {
                lcam.cullingMask = 1 << leftEyeIndex;
                lcam.cullingMask += 1;
            }
            foreach (Camera rcam in rightEyeCams)
            {
                rcam.cullingMask = 1 << rightEyeIndex;
                rcam.cullingMask += 1;
            }
        }


        /// <summary>
        /// Culls the stereo cams for mono layering
        /// </summary>
        private void CullingMono()
        {
            foreach (Camera lcam in leftEyeCams)
            {
                lcam.cullingMask = 1 << leftEyeIndex;
                lcam.cullingMask += 1;
            }
            foreach (Camera rcam in rightEyeCams)
            {
                rcam.cullingMask = 1 << rightEyeIndex;
                rcam.cullingMask += 1;
            }
        }


        /// <summary>
        /// Disables culling on stereo cam
        /// </summary>
        private void DisableCulling()
        {
            // Disable left and right eye cams

            foreach (Camera lcam in leftEyeCams)
            {
                lcam.cullingMask = 0;
            }
            foreach (Camera rcam in rightEyeCams)
            {
                rcam.cullingMask = 0;
            }
        }

        #endregion Camera Culling



        #region Event

        protected virtual void OnVrModeSwitch()
        {
            if (VrModeManagerOnSwitch != null)         //
            {
                VrModeManagerOnSwitch(this, EventArgs.Empty);
            }
        }


        #endregion Event


        #region CheckDevice VR capable

        private void CheckVRCapable()
        {
            if (SystemInfo.supportsGyroscope == false)
            {
                forcedSingleMode = true;
                Debug.Log("No gyroscope in this system. Forcing single mode");
            }
            if (true)
            {
                GetScreenDiagonal();
            }
        }

        private void GetScreenDiagonal()
        {
            //Trace.Log("XDPI: " + DisplayMetricsAndroid.XDPI);
           // Trace.Log("YDPI: " + DisplayMetricsAndroid.YDPI);
        }

        #endregion
    }
}