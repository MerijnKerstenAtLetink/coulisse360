﻿using UnityEngine;
using System.Collections;

namespace LetinkDesign.VR
{
    public class VRButtonGazer : MonoBehaviour
    {

        #region Properties

        public GazeInputModule gazeInput;
        private GameObject go;
        private VRButton vrButton;
        private VRButton lastVRButton;
        [SerializeField]
        private Renderer pointer;
        private bool pointerActive = false;
        private Color pointerColor;

        #endregion Properties


        #region Init

        void Start()
        {
            if(pointer != null)
            pointerColor = pointer.material.color;
        }

        #endregion Init


        #region Update

        void Update()
        {
            go = gazeInput.GetVRButton();

            if(go != null)
            {
                if(SceneManager.instance.isSwitching == false)
                {
                    vrButton = go.GetComponent<VRButton>();
                } 
            }
            else
            {
                vrButton = null;
            }

            if (vrButton != null)
            {
                
                //Debug.Log("VRButton: " + vrButton.name);
                vrButton.HoldGaze();
                lastVRButton = vrButton;
                pointerActive = true;
            }
            else
            {
                if(lastVRButton != null)
                {
                    lastVRButton.LostGaze();
                    lastVRButton = null;
                    pointerActive = false;
                }
            }

            if(pointer != null)
            {
                if (pointerActive)
                {
                    // use fader pointer
                    //if (pointerColor.a < 1f)
                    // {
                    //     pointerColor.a += Time.deltaTime;
                    //     pointer.material.color = pointerColor;
                    // }
                    pointer.gameObject.SetActive(true);
                    
                }
                else 
                {
                    // use fader pointer
                    // if (pointerColor.a > 0f)
                    // {
                    //     pointerColor.a -= Time.deltaTime / 2f;
                    //     pointer.material.color = pointerColor;
                    // }

                    pointer.gameObject.SetActive(false);
                }
            }
        }

        #endregion Update

    }
}
