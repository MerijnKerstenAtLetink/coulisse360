﻿using UnityEngine;
using System.Collections;
using TreeNutsGames.Trace;

namespace LetinkDesign.VR
{
    public class AreaSwitchButton : VRButton
    {
        public string areaName;
        public bool denyAcivateOnSwipe;

        public void ActivateVRButton()
        {

            if(denyAcivateOnSwipe && VrModeManager.instance.isMovingCam == true)
            {
                return;
            }

            AreaSwitchLoaderCheck check = GetComponent<AreaSwitchLoaderCheck>();
           // Trace.Log(areaName);


            if (check != null)
            {
                if (check.myState == FileState.Ready)
                {
                    if (!string.IsNullOrEmpty(areaName))
                    {
                        SceneManager.instance.ToScene(areaName);
                    }
                }
                else if(check.myState == FileState.Online)
                {
                    if(!string.IsNullOrEmpty(areaName))
                    {
                        check.LoadAsset();
                    }
                }
                else if (check.myState == FileState.Unknown)
                {
                    check.Reset();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(areaName))
                {
                    SceneManager.instance.ToScene(areaName);
                }
            }
        }
    }
}
