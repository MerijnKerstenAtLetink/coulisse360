﻿using UnityEngine;
using System.Collections;
using TreeNutsGames.Trace;

namespace LetinkDesign.VR
{
    public class VRButtonAction : VRButton
    {
        public GameObject actionObject;
        public string actionName;

        public void ActivateVRButton()
        {
           // Trace.Log("Action = " + actionName);
            
            actionObject.SendMessage(actionName);
        }
    }
}

