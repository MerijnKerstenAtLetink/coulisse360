﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using LetinkDesign.UI;
using TreeNutsGames.Trace;

namespace LetinkDesign.VR
{
    public class SceneManager : MonoBehaviour
    {

        #region Properties
        public YoutubeEasyMovieTexture youtube;

        public static SceneManager instance;

        // override texture for left and right eye
        public Texture2D leftEyeTexture;
        public Texture2D rightEyeTexture;


        public List<SceneGroup> newWorldList = new List<SceneGroup>();

        [Serializable]
        public class SceneGroup
        {
            public string name;
            public Scene[] sceneList;
        }

        // cached currentScene
        [HideInInspector]
        public Scene currentScene;

        [Serializable]
        public class Scene
        {
            public string name;
            public GameObject[] sceneObjects;
            public AssetLocationType assetType;
            public string leftAssetID;
            public string rightAssetID;
            public SceneType sceneType;
            public ModeProperty modeProperty;
            public bool adjustSpheres;
            public Vector3 startPoint;
            public bool forceStartRotationToCam;
            public float addYRotation;
            public float walkSpeed;
        }

        private bool waitForFaderHalfWay = true;
        private bool waitForAssetLoaderReady = true;

        [SerializeField]
        private Transform camPivot;

        public bool isSwitching = false;

        public List<string> visited = new List<string>();


        #region Event Delegates

        // 1- Define a delegate
        // 2- Define an event based on that delegate
        // 3- Raise the event

        //1
        public delegate void SceneManagerStartSwitchEventHandler(SceneManager sceneManager, EventArgs e);  // (object sender, EventArgs e)

        //2
        public event SceneManagerStartSwitchEventHandler StartSceneSwitch;

        #endregion Even Delegates


        #endregion Properties



        #region Init

        private void Awake()
        {
            instance = this;
            if (camPivot == null)
            {
                camPivot = Camera.main.transform.parent;
            }
            DisableAllScenes();
        }

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            // Subscribe to the FaderAtHalf
            UIFader.instance.FaderAtHalf += this.OnFaderAtHalf;

            if (newWorldList.Count > 0 && newWorldList[0].sceneList.Length > 0)
            {
                StartCoroutine(ToScene(newWorldList[0].sceneList[0]));
            }
            if (newWorldList.Count > 0)
            {
                //Debug.Log("WorldList: " + newWorldList.Count);
            }
        }

        #endregion Init



        #region SceneSwitching
        

        /// <summary>
        /// Seeks the scene by name and starts switching
        /// </summary>
        /// <param name="sceneName"></param>
        public void ToScene(string sceneName)
        {
            if(!isSwitching && UIFader.busy == false)
            {
                isSwitching = true;
                sceneName = sceneName.ToLower();

                if (newWorldList.Count > 0 && newWorldList[0].sceneList.Length > 0)
                {
                    Scene nextScene = new Scene();

                    foreach (SceneGroup group in newWorldList)
                    {
                        foreach (Scene scene in group.sceneList)
                        {
                            scene.name = scene.name.ToLower();
                            if (scene.name == sceneName)
                            {
                                nextScene = scene;
                                break;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(nextScene.name))
                    {
                        StartCoroutine(ToScene(nextScene));
                    }
                    else
                    {
                        Debug.LogError("Could not find scene: " + sceneName);
                        StartCoroutine(ToScene(newWorldList[0].sceneList[0]));
                    }
                }
                else
                {
                    Debug.LogError("SceneManager.worldList.sceneList has no scenes");
                }
            }
            else
            {
                //Trace.Log("Allready busy");
            }
        }


        private IEnumerator ToScene(Scene nextScene)
        {

            OnStartSceneSwitch();

            // Start the fader
            waitForFaderHalfWay = true;
            waitForAssetLoaderReady = true;
            UIFader.instance.StartFadeInOut(2f);

            // Wait for the fader to be 100% black
            while (waitForFaderHalfWay)
            {
                Debug.Log("IM stuck here");
                yield return null;
            }


            #region HALFWAY EVENT
            // The screen is now 100% black


            #region Closing LastScene
            DisableAllScenes();
            UnLoadCurrentImages();

            #endregion Closing LastScene

            yield return null;

            #region Setup NextScene

            bool isVideo = IsVideoScene(nextScene);

            if (isVideo)
            {
                PanoramicManager.instance.InitPanoramic(nextScene);

                switch (nextScene.assetType)
                {
                    case AssetLocationType.Unknown:
                        Debug.Log("Scene setup warning !");
                        break;
                    case AssetLocationType.StreamingAsset:
                        PanoramicManager.instance.videoPlayer.Load(nextScene.leftAssetID);
                        yield return new WaitForSeconds(0.5f);
                        PanoramicManager.instance.videoPlayer.Play();
                        break;
                    case AssetLocationType.Downloadable:
                        string newPath = AssetLoader.instance.assetDirectory + nextScene.leftAssetID;
                        Debug.Log("VIDEO SETUP: " + newPath);
                        PanoramicManager.instance.videoPlayer.LoadLocalFile(newPath);
                        yield return new WaitForSeconds(0.5f);
                        PanoramicManager.instance.videoPlayer.Play();
                        break;
                    case AssetLocationType.Youtube:
                        youtube.youtubeVideoIdOrUrl = nextScene.leftAssetID;
                        youtube.LoadYoutubeInTexture();
                        yield return new WaitForSeconds(2f);
                        PanoramicManager.instance.videoPlayer.Play();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(nextScene.leftAssetID))
                {
                    //Debug.Log("AssetLoader.Load(nextScene.leftMaterialID");
                    leftEyeTexture = AssetLoader.instance.LoadTexture(nextScene.leftAssetID);
                    PanoramicManager.instance.LeftPano(leftEyeTexture);
                }

                if (!string.IsNullOrEmpty(nextScene.rightAssetID))
                {
                    //Debug.Log("AssetLoader.Load(nextScene.rightMaterialID");
                    rightEyeTexture = AssetLoader.instance.LoadTexture(nextScene.rightAssetID);
                    PanoramicManager.instance.RightPano(rightEyeTexture);
                }
            }

            // enable next scene gameObjects
            EnableNextScene(nextScene);


            yield return null;

            if (isVideo == false)
            {
                PanoramicManager.instance.InitPanoramic(nextScene);
            }


            VrModeManager.instance.InitVR(nextScene);

            yield return null;

            #endregion Setup NextScene

            #endregion HALFWAY EVENT

            // sets the new scene as currentscene
            currentScene = nextScene;

            isSwitching = false;

            AddToVisited(currentScene.name);

        }

        private static bool IsVideoScene(Scene nextScene)
        {
            bool isVideo = false;


            switch (nextScene.sceneType)
            {
                case SceneType.Panoramic:
                    break;
                case SceneType.PanoramicStereoDualMono:
                    break;
                case SceneType.PanoramicStereoTopDown:
                    break;
                case SceneType.PanoramicStereoSideBySide:
                    break;
                case SceneType.VideoMono:
                    isVideo = true;
                    break;
                case SceneType.VideoStereoTopDown:
                    isVideo = true;
                    break;
                case SceneType.VideoStereoSideBySide:
                    isVideo = true;
                    break;
                case SceneType.ThreeD:
                    break;
                default:
                    break;
            }

            return isVideo;
        }

        private void EnableNextScene(Scene nextScene)
        {
            foreach (GameObject go in nextScene.sceneObjects)
            {
                go.SetActive(true); 
            }
        }

        private void UnLoadCurrentImages()
        {
            if (currentScene != null)
            {
                if (!string.IsNullOrEmpty(currentScene.leftAssetID))
                {
                    Debug.Log("Left material garbage collect !");
                    if(leftEyeTexture != null)
                    {
                        leftEyeTexture = null;
                    }
                }
                if (!string.IsNullOrEmpty(currentScene.rightAssetID))
                {
                    Debug.Log("Right material garbage collect !");
                    if(rightEyeTexture != null)
                    {
                        rightEyeTexture = null;
                    }
                }
            }
            else
            {
                Debug.Log("Can't unload currentScene");
            }
            PanoramicManager.instance.Clear();

            Resources.UnloadUnusedAssets();

        }


        private void DisableAllScenes()
        {
            foreach (SceneGroup group in newWorldList)
            {
                foreach (Scene scene in group.sceneList)
                {
                    foreach (GameObject go in scene.sceneObjects)
                    {
                        if(go != null)
                        {
                            go.transform.localEulerAngles = Vector3.zero;
                            go.SetActive(false);
                        }
                        else
                        {
                            Debug.Log("SceneManager can't dissable this objects ["+ scene.name + "]");
                        }
                    }
                }
            }
        }

        private void AddToVisited(string name)
        {
            name = name.ToLower();
            if(visited.Count > 0)
            {
                if(name != visited[0])
                {
                    visited.Insert(0, name);
                }
            }
            else
            {
                visited.Insert(0, name);
            }
        }

        public void NavigateBack()
        {
            if (!isSwitching)
            {
                if (visited.Count > 0)
                {
                    if (visited.Count > 1)
                    {
                        visited.Remove(visited[0]);
                    }
                    ToScene(visited[0]);
                }
            }
        }

        #endregion SceneSwitching



        #region Assets

        public string AssetID(string areaName)
        {
            areaName = areaName.ToLower();

            string ret = "";
            foreach (SceneGroup group in newWorldList)
            {
                foreach (Scene scene in group.sceneList)
                {
                    if (scene.name.ToLower() == areaName.ToLower())
                    {
                        ret = scene.leftAssetID;
                        break;
                        // no right AssetID yet !
                    }
                }
            }
            if(ret == "")
            {
                Debug.LogWarning("No assetID found for area: " + areaName);
            }

            return ret;
        }


        #endregion Assets



        #region EventHandling


        /// <summary>
        /// Is called by eventhandler delegate when fader is @ half
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnFaderAtHalf(UIFader sender, EventArgs e) // (object source, EventArg e)
        {
            //Debug.Log("SceneManager heard the fader is at halfWay");
            waitForFaderHalfWay = false;
        }


        protected virtual void OnStartSceneSwitch()
        {
            if (StartSceneSwitch != null)         //
            {
                StartSceneSwitch(this, EventArgs.Empty);
            }
        }

        #endregion EventHandling

    }
}
