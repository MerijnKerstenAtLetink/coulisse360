﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SmartScreenShot : MonoBehaviour
{

    public int currentScreenshot = 0;
    public int screenshotCount = 0;
    public string savingpath = "D:/";   //example:   E:/MyScreenshots
    public string screenshotName = "Screenshot";

    public List<ScreenShotSets> ScreenSets = new List<ScreenShotSets>();


    //private float dFactor = 1.0f;
    private int dFactor = 1;

    [System.Serializable]
    public class ScreenShotSets
    {
        public string name = "";
        public bool include = false;
        public int width = 600;
        public int height = 360;
    }


    void Update()
    {
        if (Input.GetKeyUp("/"))
        {
            TakeSingleShot();
            //TakeAllShots();
        }
    }


    void TakeSingleShot()
    {
        //      F:/Screenshots/Ios_3.5inch_T01_01.png
        dFactor = DFactor();

        string deviceName = ScreenSets[currentScreenshot].name;
        string screenSize = ScreenSets[currentScreenshot].width + "X" + ScreenSets[currentScreenshot].height;
        string scrNm = savingpath + "/" + screenshotName + "_" + screenshotCount + "_" + deviceName + "_" + Application.loadedLevelName + "_" + screenSize + ".png";

        Application.CaptureScreenshot(scrNm, dFactor);
        Debug.Log(scrNm);
        screenshotCount++;
    }

    private int DFactor()
    {
        //float ret = 1.0;
        int ret = 1;

        if (Screen.width < ScreenSets[currentScreenshot].width)
        {
            dFactor = Screen.width / ScreenSets[currentScreenshot].width;
            //ret = dFactor;
            ret = 2;  // Application.CaptureScreenshot  cant take float,  to bad  cause i would have been a conventient scaleFactor otherwise;
            Debug.Log("Screenwidth is smaller then screenshotwith size, setting scaling factor to: " + dFactor.ToString());
        }
        else
        {
            Debug.Log("DFactor = " + dFactor);
        }

        return ret;
    }
}