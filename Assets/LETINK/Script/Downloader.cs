﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using System.Net.Sockets;
using System;
using LetinkDesign;
using LetinkDesign.VR;

public class Downloader : MonoBehaviour {

    [SerializeField]
    private string host = "www.virtualletink.nl"; // www.virtualletink.nl
    [SerializeField]
    private string projectFolder = "/samfeldt";  //  /samfeldt
    [SerializeField]
    private string assetDirectory;
    private string serverQualityFolder;
    [SerializeField]
    private string uri = "";
    private string assetID = "";
    private FileStream fileStream;

    [SerializeField]
    private int bufferSize = 16; // in Kb

    internal IEnumerator RequestStreamDownload(string assetID, object sender)
    {

        switch (Settings.instance.playerSettings.videoQuality)
        {

            case Settings.VideoQuality.Test:
                serverQualityFolder = "test/";
                break;
            case Settings.VideoQuality.Low:
                serverQualityFolder = "low/";
                break;
            case Settings.VideoQuality.Medium:
                serverQualityFolder = "medium/";
                break;
            case Settings.VideoQuality.High:
                serverQualityFolder = "high/";
                break;
            default:
                break;
        }



        assetDirectory = Application.persistentDataPath + "/assets/" + serverQualityFolder;

        uri = "/" + projectFolder + serverQualityFolder + assetID;

        string query = "GET " + uri.Replace(" ", "%20") + " HTTP/1.1\r\n" +
                         "Host: " + host + "\r\n" +
                         "User-Agent: undefined\r\n" +
                         "Connection: close\r\n" +
                         "\r\n";


        Debug.Log(query);

        uint contentLength;
        int n = 0;
        int read = 0;

        NetworkStream networkStream;
        fileStream = null;
        Socket client;


        client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
        client.Connect(host, 80);

        networkStream = new NetworkStream(client);

        var bytes = Encoding.Default.GetBytes(query);
        networkStream.Write(bytes, 0, bytes.Length);

        var bReader = new BinaryReader(networkStream, Encoding.Default);

        string response = "";
        string line;
        char c;

        do
        {
            line = "";
            c = '\u0000';
            while (true)
            {
                c = bReader.ReadChar();
                if (c == '\r')
                    break;
                line += c;
            }
            c = bReader.ReadChar();
            response += line + "\r\n";
        }
        while (line.Length > 0);

        Debug.Log(response);

        Regex reContentLength = new Regex(@"(?<=Content-Length:\s)\d+", RegexOptions.IgnoreCase);
        contentLength = uint.Parse(reContentLength.Match(response).Value);

        fileStream = new FileStream(assetDirectory  + assetID, FileMode.Create);

        
        AreaSwitchLoaderCheck asSender = sender as AreaSwitchLoaderCheck;
        GameObject goSender = asSender.GetComponent<Transform>().gameObject;
        AssetLoader.instance.isDownloading = true;

        byte[] buffer = new byte[bufferSize * 1024];

        while (n < contentLength)
        {
            if (networkStream.DataAvailable)
            {
                read = networkStream.Read(buffer, 0, buffer.Length);
                n += read;
                fileStream.Write(buffer, 0, read);
            }
            // Debug.Log("Downloaded: " + n + " of " + contentLength + " bytes ...");
            float percent = (1f / contentLength * (n));

            
            if(sender != null && goSender.activeInHierarchy)
            {
                asSender.UpdateProgress(percent);
                if(percent == 1)
                {
                    asSender.Reset();
                }
            }
            yield return null;
        }
        

        yield return null;
        Release();
        client.Close();
        fileStream = null;
      
        AssetLoader.instance.isDownloading = false;
        sender = null;
    }

    private void Release()
    {
        if(fileStream != null)
        {
            fileStream.Flush();
            fileStream.Close();
        } 
    }

    void OnDestroy()
    {
        Release();
    }
}
