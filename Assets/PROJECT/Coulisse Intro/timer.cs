﻿using UnityEngine;
using System.Collections;

public class timer : MonoBehaviour {

    [SerializeField]
    private float wijzer;
    public GameObject StartInstructionScreen;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        wijzer -= Time.deltaTime;

        if (wijzer <= 0.1f)
        {
            StartInstructionScreen.GetComponent<LoadScene>().LoadSceneNow();
        }
    }
}
