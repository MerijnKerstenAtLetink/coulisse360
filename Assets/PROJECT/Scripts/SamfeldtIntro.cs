﻿using UnityEngine;
using System.Collections;
using System;
using TreeNutsGames.Trace;
using LetinkDesign.UI;
using UnityEngine.UI;

public class SamfeldtIntro : MonoBehaviour {

    [SerializeField]
    private float movieLength = 18f;


    [SerializeField]
    private GameObject SkipButton;

    [SerializeField]
    private GameObject Fullscreen;

    [SerializeField]
    private GameObject StartInstructions;

    [SerializeField]
    private UIFader fader;
	
	void Start ()
    {
        StartCoroutine(WaitForIntroMovie());
	}

    private IEnumerator WaitForIntroMovie()
    {
        UIFader.instance.StartFadeIn(1f);

        yield return new WaitForSeconds(movieLength);

        SkipIntro();
    }

    public void SkipIntro()
    {
        StopAllCoroutines();
        //Trace.Log("Done!");
        StartCoroutine(SkippingIntro());
        OneSignal.RegisterForPushNotifications();
    }

    private IEnumerator SkippingIntro()
    {
        SkipButton.GetComponent<Button>().interactable = false;

        if(fader)
        {
            fader.StartFadeOut(1f);
        }

        yield return new WaitForSeconds(0.5f);
        SkipButton.SetActive(false);
        yield return new WaitForSeconds(0.5f);

        Fullscreen.SetActive(false);
        StartInstructions.SetActive(true);
        fader.StartFadeIn(1f);
    }
}
