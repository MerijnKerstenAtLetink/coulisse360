﻿using UnityEngine;
using System.Collections;
using System;
using LetinkDesign;

public class CamSmoother : MonoBehaviour
{


    public static CamSmoother instance;

    [SerializeField]
    private float dampening;
    [SerializeField]
    private Transform damper;
    [SerializeField]
    private Transform head;
    [SerializeField]
    private Transform cam;

  //  [SerializeField]
  //  private Transform cardboardRoot;


	void Awake ()
    {
        instance = this;
	}

    private IEnumerator Start()
    {
        while(GameMain.inited == false)
        {
            yield return null;
        }
        dampening = Settings.instance.playerSettings.singleScreenDamper;
        if(dampening <= 0f)
        {
            dampening = 5f;
        }
    }
	
	
	void Update ()
    {
        Quaternion dampedRot;

        dampedRot = Quaternion.Lerp(damper.rotation, head.rotation , dampening * Time.deltaTime);
        damper.rotation = dampedRot;
	}



    [ContextMenu("StartDamper")]
    public void StartDamper()
    {
        cam.parent = damper;
        cam.localEulerAngles = Vector3.zero;
        this.enabled = true;
    }

    [ContextMenu("StopDamper")]
    public void StopDamper()
    {
        this.enabled = false;
        cam.parent = head;
        cam.localEulerAngles = Vector3.zero;
    }

    internal void UseDamp(bool useDamper)
    {
        if(useDamper)
        {
            StartDamper();
        }
        else
        {
            StopDamper();
        }
    }
}
