﻿using UnityEngine;
using System.Collections;

public class YoutubeEasyMovieTexture : MonoBehaviour
{

    public string youtubeVideoIdOrUrl;
    private MediaPlayerCtrl mediaPlayer;

    void Awake()
    {
        mediaPlayer = GetComponent<MediaPlayerCtrl>();
    }

	void Start ()
    {
        
	}

    public void LoadYoutubeInTexture()
    {
        //ALERT If you are using EasyMovieTexture uncomment the line..
        //mediaPlayer.m_strFileName = YoutubeVideo.Instance.RequestVideo(youtubeVideoIdOrUrl,720);
        mediaPlayer.Load(YoutubeVideo.Instance.RequestVideo(youtubeVideoIdOrUrl, 720));
    }
}
