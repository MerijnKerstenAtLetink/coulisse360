﻿using UnityEngine;
using System.Collections;
using TreeNutsGames.Trace;



public class SphereMirror : MonoBehaviour
{

    private static SphereMirror instance;

    [SerializeField]
    private bool mirrorAtStart = false;

    [SerializeField]
    private Transform[] uvAtStartFix;


    void Awake()
    {
        instance = this;
    }



    void Start()
    {
#if UNITY_IOS || UNITY_TVOS || UNITY_EDITOR || UNITY_STANDALONE
        if(mirrorAtStart)
        {
            foreach (var item in uvAtStartFix)
            {
                MirrorUV(item);
            }
        }
#endif
    }


    public static void MirrorSpheres(Transform uvTransform)
    {
        instance.MirrorUV(uvTransform);
    }


    private void MirrorUV(Transform uvTransform)
    {
        Vector2[] vec2UVs = uvTransform.GetComponent<MeshFilter>().mesh.uv;
        //Trace.Log("Reversing " + uvTransform.GetComponent<MeshFilter>().mesh.ToString());

        for (int i = 0; i < vec2UVs.Length; i++)
        {
            vec2UVs[i] = new Vector2(vec2UVs[i].x, 1.0f - vec2UVs[i].y);
        }

        uvTransform.GetComponent<MeshFilter>().mesh.uv = vec2UVs;
    }

}
