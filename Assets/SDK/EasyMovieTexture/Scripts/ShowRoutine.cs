﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ShowRoutine : MonoBehaviour
{

    int index = 0;

    public List<GameObject> groups;


    public void Next()
    {
        index++;
        SetNext();
    }

    public void Back()
    {
        index--;
        SetNext();
    }

    private void SetNext()
    {
        DisabbleAll();

        if(index > groups.Count -1)
        {
            index = 0;
        }
        else if(index < 0)
        {
            index = groups.Count -1;
        }

        groups[index].SetActive(true);
        
    }

    private void DisabbleAll()
    {
        foreach (GameObject go in groups)
        {
            go.SetActive(false);
        }
    }
}
